<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php
/* Control para que no puedan ingresar directaemente a la URL sin estar logueado
*/
  if (empty($_SESSION['is_logged_in'])){
    session_destroy();
    redirect();
  }else
    {
?>
        <div class="contenedor">
          <div class="card" style="width: 50rem;">
            <div class="card-header text-center text-white bg-dark ">
              <h1>Muro de <?php echo $_SESSION['nomuser'] ?></h1>
            </div>

            <?php echo form_open("addpost"); ?>
            <div class="mb-3">
              <input type="text" class="form-control" name='titulo' id="validationDefault04" placeholder="Titulo" required>
              <input type="text" class="form-control" name='compartir' id="validationDefault04" placeholder="Compartir" required>
            </div>
            <button class="btn btn-primary" type="submit" style="width: 10rem;">Compartir</button>
            <?php echo form_close(); ?>
            <!--
            <div class="card-body">
              <div class="custom-file">
                <input type="file" class="custom-file-input" id="inputGroupFile01">
                <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
              </div>
            </div>
            // Boton-opciones-publicar
            <div class="input-group">
              <input type="text" class="form-control" aria-label="Text input with segmented dropdown button">
              <div class="input-group-append">
                <button type="button" class="btn btn-primary">Compartir</button>
                <button type="button" class="btn btn-outline-secondary dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <span class="sr-only">Toggle Dropdown</span>
                </button>
                <div class="dropdown-menu">
                  <a class="dropdown-item" href="#">Compartir Amigos</a>

                  <a class="dropdown-item" href="#">Compartir Publico</a>
                  <a class="dropdown-item" href="#">Compartir Privado</a>
                  <div role="separator" class="dropdown-divider"></div>
                  <a class="dropdown-item" href="#">Borrar</a>
                </div>

              </div>
            </div>
          -->
          <!--

          -->
          </div>
          <!-- contenido de muro-->
          <div class="card" style="width: 50rem;">
            <div class="list-group" id="datMuro">
              <!--
              Aqui se carga el contendio del muro con AJAX
            -->
          </div>
        </div>
        </div>
        <?php
      };
        ?>
