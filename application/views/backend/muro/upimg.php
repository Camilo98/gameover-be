<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>


<div class="contenedor">
<div class="card" style="width: 50rem;">
  <div class="card-header text-center text-white bg-dark ">
    <h1>Subir imagenes</h1>
  </div>
<!--form_open_multipart es un formulario para subir archivos  -->
  <?php echo form_open_multipart("subirImagen"); ?>
  <div class="mb-3">
    <div class="form-group">
      <label for="exampleFormControlFile1">Selecione una imagen</label>
      <input type="file" class="form-control-file" name='fileimg' id="exampleFormControlFile1" required>
    </div>

<button type="submit" class="btn btn-primary" style="width: 10rem;">Subir</button>

  <?php echo form_close(); ?>

</div>
  <div class="card">


  </div>
</div>
