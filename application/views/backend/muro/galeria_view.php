<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php

/* Control para que no puedan ingresar directaemente a la URL sin estar logueado
*/
  if (empty($_SESSION['is_logged_in'])){
    session_destroy();
    redirect();

  }elseif ($_SESSION['rol'] == 'General Admin') {
      redirect(base_url().'padmin'); 
      # code...
    }
   
    
  elseif ($_SESSION['rol'] == 'User Admin') {
    redirect(base_url().'listuser');
    
  }elseif ($_SESSION['rol'] == 'Subscription Admin') {
    $this->session->set_flashdata('mensaje_error', 'No tiene privligios para ingresar');
    redirect(base_url().'gest_suscription');
  }else{
?> 

<div class="contenedor">
<div class="card" style="width: 50rem;">
  <div class="card-header text-center text-white bg-primary ">
    <h1>Subir imagenes</h1>
  </div>
<!--form_open_multipart es un formulario para subir archivos  -->
<?php echo form_open_multipart("galeria/compartir"); ?>
<div class="mb-3">
  <div class="form-group">
    <label for="fileimg">Selecione una imagen</label>
    <input type="file" class="form-control-file" name='fileimg' id="fileimg" required>
  </div>
  <div class="mb-3">
    <textarea type="text" class="form-control" name='compartir' id="validationDefault04" placeholder="Comentario"></textarea>
    <input name="url" type="hidden" value="<?php echo $this->uri->segment(1); ?>">
  </div>
  <button class="btn btn-primary" type="submit" style="width: 10rem;">Subir</button>
</div>
<?php echo form_close(); ?>

</div>
</div>
<section class="gallery-block grid-gallery">
            <div class="contenedor ">
  <div  id="datImg" class="row">
    
              
    <!--
    * aqui mustra las imagenes con JSON
    *
    -->
 
</div>
</div>

</section>

<?php
};
?>