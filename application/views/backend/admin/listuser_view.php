<?php
  defined('BASEPATH') OR exit('No direct script access allowed');
  ?>

  <?php

/* Control para que no puedan ingresar directaemente a la URL sin estar logueado
*/
  if (empty($_SESSION['is_logged_in'])){
    session_destroy();
    redirect();

  }elseif ($_SESSION['rol'] == 'General Admin') {
    $this->session->set_flashdata('mensaje_error', 'No tiene privligios para ingresar');
      redirect(base_url().'padmin'); 
      
      
    }
   
    
  elseif ($_SESSION['rol'] == 'Publicity Admin') {
    $this->session->set_flashdata('mensaje_error', 'No tiene privligios para ingresar');
    redirect(base_url().'galeria');

  }elseif ($_SESSION['rol'] == 'Subscription Admin') {
    $this->session->set_flashdata('mensaje_error', 'No tiene privligios para ingresar');
    redirect(base_url().'gest_suscription');
  }else{
?> 

  
      
      <div class="container">
        <div id="alert">
        
      </div>
      <div class="dropdown">
      <div class="btn-group" style="flex; margin-right: 20px;">
  <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    Mostrar Usuarios:
  </button>
  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
    <a class="dropdown-item" id='enable' href="#">Habilitados</a>
    <a class="dropdown-item" id='disable' href="#">Deshabilitados</a>
   </div>
</div>
</div>
<br>



<div class="card">
         <div class="card-header text-center text-white bg-primary ">
            <h2>USUARIOS del sistema</h2>
          </div>
  <div class="card-body">
            <blockquote class="blockquote mb-0">
              <?php echo form_open('padmin/buscar');?>
                <div class="form-group">
                  <input type="text" name="search" value="<?php echo set_value('cuenta'); ?>" class="form-control" id="search" placeholder="Buscar Usuario">
                </div>
                
              <!--</form>-->
            </blockquote>
          </div>
  <!--<div class="container">-->
<div class="card-body">
      <table class="table-responsive">
        <thead class="thead-dark table">
          <tr class="table-primary">
            <th>
                       <div class="modal fade" id="Confirmacion_1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                      <div class="modal-dialog" role="document">
                      <div class="modal-content">
                      <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel" style="color: #000">Confirmacion</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                     <span aria-hidden="true">&times;</span>
                     </button>
                     </div>
                     <div class="modal-body">
                     <p style="color: #000">Esta seguro que quiere<span id="myText"></span>este usuario?</p>
                     </div>
                     <div class="modal-footer">
                     <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        
                    <button name="Deshabilitar" id="Deshabilitarbtn"  type="button" class="btn btn-primary" data-toggle="modal" data-target="#Confirmacion_1">
                    Si, Deshabilitar
                    </button>
                                 
        
                    </div>
                    </div>
                    </div>
                    </div>
                    <button  type="button" class="btn btn-primary btnloco" data-toggle="modal" data-target="#Confirmacion_1">
                    Deshabilitar
                    </button>

                      
                    <br><br>
                    <div class="modal fade" id="Confirmacion" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                    <div class="modal-content">
                    <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel" style="color: #000">Confirmacion</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                    </div>
                    <div class="modal-body">
                    <p style="color: #000">Esta seguro que quiere Eliminar este usuario?</p>
                    </div>
                    <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        
                    <button name="Eliminar"  id="Eliminarbtn" type="button" class="btn btn-primary" data-toggle="modal" data-target="#Confirmacion">
                    Si, Eliminar
                    </button>
                                 
        
                    </div>
                    </div>
                    </div>
                    </div>
                    <button  type="button" class="btn btn-primary" data-toggle="modal" data-target="#Confirmacion" >
                    Eliminar
                    </button>
                    </th>
          <th scope="col">#</th>
          <th scope="col">Mail </th>
          <th scope="col">Nombre </th>
          <th scope="col">Celular</th>
          <th scope="col">Fecha Nac.</th>
          <th scope="col">Apellido</th>
          <th scope="col">Estado</th>
          <th scope="col">Sexo</th>
          

        </tr>
        <!--
        **
        ** Listo los usuarios en una tabla con el array que envio desde el controlador listuser
        **
        **
      -->
    </thead>

    <tbody id='listar_usuarios'>
     

    </tbody>
  </table>

  </div>
  </div>
  </div>

  <h1>PAGINAS</h1>


  <div class="container">
    <table class="table">
      <thead class="thead-dark">
        <tr>
          <th>
                       <div class="modal fade" id="Confirmacion_1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                      <div class="modal-dialog" role="document">
                      <div class="modal-content">
                      <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel" style="color: #000">Confirmacion</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                     <span aria-hidden="true">&times;</span>
                     </button>
                     </div>
                     <div class="modal-body">
                     <p style="color: #000">Esta seguro que quiere<span id="myText2"></span>este usuario?</p>
                     </div>
                     <div class="modal-footer">
                     <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        
                    <button name="Deshabilitar" id="Deshabilitarbtn"  type="button" class="btn btn-primary" data-toggle="modal" data-target="#Confirmacion_1">
                    Si, Deshabilitar
                    </button>
                                 
        
                    </div>
                    </div>
                    </div>
                    </div>
                    <button  type="button" class="btn btn-primary btnloco2" data-toggle="modal" data-target="#Confirmacion_1">
                    Deshabilitar
                    </button>

                      
                    <br><br>
                    <div class="modal fade" id="Confirmacion" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                    <div class="modal-content">
                    <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel" style="color: #000">Confirmacion</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                    </div>
                    <div class="modal-body">
                    <p style="color: #000">Esta seguro que quiere Eliminar este usuario?</p>
                    </div>
                    <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        
                    <button name="Eliminar"  id="Eliminarbtn" type="button" class="btn btn-primary" data-toggle="modal" data-target="#Confirmacion">
                    Si, Eliminar
                    </button>
                                 
        
                    </div>
                    </div>
                    </div>
                    </div>
                    <button  type="button" class="btn btn-primary" data-toggle="modal" data-target="#Confirmacion" >
                    Eliminar
                    </button>
                    </th>
          <th scope="col">#</th>
          <th scope="col">Mail </th>
          <th scope="col">Nombre </th>
          <th scope="col">Celular</th>
          <th scope="col">Fecha Nac.</th>
          <th scope="col">Pagina Web</th>
          <th scope="col">Estado</th>
         
          
        </tr>
        <!--
        **
        ** Listo los usuarios en una tabla con el array que envio desde el controlador listuser
        **
        **
      -->
    </thead>
    <tbody id='listar_paginas'>
     
    </tbody>
  </table>
</div>
  
  
 <?php }; ?>