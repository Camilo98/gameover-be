<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<?php

/* Control para que no puedan ingresar directaemente a la URL sin estar logueado
*/
  if (empty($_SESSION['is_logged_in']) or $_SESSION['rol'] != 'General Admin'){
    session_destroy();
    redirect();
  }else
    {
?>
<div class="container">

  <?php echo form_open('padmin/addUser'); /*
    *en vez de escribir todo esto
    *(<form action="<?php echo base_url(); ?>adduser" method='POST'>)
    **/
  ?>
  <div class="card" style="width: 60rem;">
   <div class="card-header text-center text-white bg-primary ">
      <h2>Registro</h2>
    </div>
    <div class="form-row">
      <div class="col-md-4 mb-3">
        <label for="username">Username</label>
        <input type="text" class="form-control" name='username' value="<?php echo set_value('username'); ?>" id="username" placeholder="Nombre" required>
      </div>
      <div class="col-md-4 mb-3">
        <label for="nombre">Nombre</label>
        <input type="text" class="form-control" name='nombre' value="<?php echo set_value('nombre'); ?>" id="nombre" placeholder="Nombre" required>
      </div>
     <div class="col-md-4 mb-3">
        <label for="correo">Correo</label>
        <div class="input-group">
        <input type="email" class="form-control" name='correo' value="<?php echo set_value('correo'); ?>" id="correo" placeholder="correo@abc.com" aria-describedby="inputGroupPrepend2" required>
      </div>
      </div>
     
     </div>
    <div class="form-row">
      <div class="col-md-4 mb-3">
        <label for="pass">Contraseña</label>
        <input type="password" name="pass" class="form-control" id="pass" placeholder="Ingrese su Contraseña" required>
      </div>
      <div class="col-md-4 mb-3">
        <label for="repass">Repite Contraseña</label>
        <input type="password" name="repass" class="form-control" id="repass" placeholder="Repita Contraseña" required>
      </div>
      
       <div class="col-md-4 mb-3">
        <label for="apellido">Apellido</label>
        <div class="input-group">
          <input type="text" class="form-control" name='apellido' value="<?php echo set_value('apellido'); ?>" id="apellido" placeholder="Apellido" aria-describedby="inputGroupPrepend2" required>
        </div>
        </div>
        </div>
      <div class="form-row">
      <div class="col-md-4 mb-3">
        <label for="rol">Rol</label>
        <select id="inputState" class="form-control" name="rol"  id="rol" placeholder="" required>
        <option>General Admin</option>
        <option>User Admin</option>
        <option>Subscription Admin</option>
        <option>Publicity Admin</option>
      </select>
     </div>
     </div>

     <div class="form-row">
        <div class="col-md-4 mb-3">

          <button class="btn btn-primary" type="submit" style="width: 10rem;">Registrarme</button>

     
      </div>

     </div>

    
  </form>
</div>

<?php
};
?>


