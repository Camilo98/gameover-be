<?php
  defined('BASEPATH') OR exit('No direct script access allowed');
  ?>

<?php

/* Control para que no puedan ingresar directaemente a la URL sin estar logueado
*/
  if (empty($_SESSION['is_logged_in'])){
    session_destroy();
    redirect();

  }elseif ($_SESSION['rol'] == 'General Admin') {
    $this->session->set_flashdata('mensaje_error', 'No tiene privligios para ingresar');
      redirect(base_url().'padmin'); 
      
      
    }elseif ($_SESSION['rol'] == 'Publicity Admin') {
    $this->session->set_flashdata('mensaje_error', 'No tiene privligios para ingresar');
    redirect(base_url().'galeria');
    
  }elseif ($_SESSION['rol'] == 'User Admin') {
    $this->session->set_flashdata('mensaje_error', 'No tiene privligios para ingresar');
    redirect(base_url().'listar');
  }else{
?> 


<!-- Example single danger button type="button" id='button' class="btn btn-primary "-->



  <div class="container " style="padding: 20px; "  >
    <div style="border-radius: 5px;" 
      class="view view-cascade gradient-card-header default-color-dark narrower py-2 mx-6 mb-5 d-flex justify-content-between align-items-center">

      <div>
         <div class="modal fade" id="exampleModalCenter2" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title text-dark" id="exampleModalLongTitle">Editar Suscripcion</h5><span>&nbsp;&nbsp;</span>

        <div id="vencimiento-alert"></div>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

          <div class="card" style="width: 30rem;">
          <div class=" card-header text-dark">
            Modificar Suscripcion
          </div>

          <ul class="list-group list-group-flush">
           <li class="list-group-item">
              <label class="text-dark" for="inputIdSuscrip">idSuscripcion</label>
              <input type="text" class="form-control" name="inputIdSuscrip" id="inputIdSuscrip" readonly value=''>
            </li>
              <li class="list-group-item">
              <label class="text-dark" for="inputID">idCuenta</label>
              <input type="text" class="form-control" name="inputID" id="inputID" readonly value=''>
            </li>
            
            <li class="list-group-item">
              <label class="text-dark" for="inputEstado">Estado</label>
              <input type="text" class="form-control" name="inputEstado" id="inputEstado" readonly value=''>
        
            </li>
            <li class="list-group-item">
              <label class="text-dark" for="inputFecIni">Fecha Inicio</label>
              <input type="text" class="form-control" name="inputFecIni" id="inputFecIni" >
            </li>
            <li class="list-group-item">
              <!--
              <label class="text-dark" for="inputFecFin">Fecha Fin</label>
              <input type="text" class="form-control" name="inputFecFin" id="inputFecFin" >
            -->
            
            
          
                <label for="inputFecFin" class="text-dark">Fecha Fin</label>
                <input size="16" type="text" name="inputFecFin" id="inputFecFin" readonly class="form_datetime">
              
             
    
            
      
            </li>

            <li class="list-group-item">
         
            
                    <label class="text-dark" for="inputType">Tipo Suscripcion</label>
                    
                    <select  id="inputType" class="form-control" name="inputType">
                    <option>3 Meses</option>
                    <option>6 Meses</option>
                    <option>1 año</option>
                    
                  </select>
                
             
            </li>
          </ul>

      </div>
      <div class="modal-footer">
        <button type="button" id='cerrar' class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button class="btn btn-primary update1" type="button" data-dismiss="modal" style="width: 10rem;">Editar</button>

      </form>
      </div>
    </div>
  </div>
</div>
</div>
        <button type="button" class=" btn btn-outline-white btn-rounded btn-sm px-10 update" data-toggle="modal" data-target-custom="#exampleModalCenter2">
        <i class="fas fa-pencil-alt mt-2">
        <span class="oi" data-glyph="pencil">
        </span>
        </i>  

        </button>
         <button id = 'CargarSus' type="button" class=" btn btn-outline-white btn-rounded btn-sm px-10">
        <i class="fas fa-pencil-alt mt-2">
        <span class="oi" data-glyph="eject">
        </span>
        </i>  

        </button>
        
      </div>

<a href="" class=" white-text mx-3">Suscripciones</a>

      
      </button>
        <button type="button" class="btn btn-outline-white btn-rounded btn-sm px-10 reload">
         
           <span class="oi" data-glyph="reload"></span>
          
        </button>

     

    </div>

     


   <table id="dt-basic-checkbox" class="table table-striped table-bordered" cellspacing="0" width="100%">
     <div class="d-flex justify-content-center" >
  <button type="button" class="d-flex btn btn-secondary dropdown-toggle btnestado " data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    Estado
  </button>
  <div class="dropdown-menu"  >
    <a class="dropdown-item suscripcion" href="#">Pendiente</a>
    <a class="dropdown-item suscripcion" href="#">Cumplido</a>
    <a class="dropdown-item suscripcion" href="#">Finalizado</a>
    <a class="dropdown-item" id="Vencimiento" href="#">Por vencer</a>
    <a class="dropdown-item suscripcion" href="#">Todos</a>
     
    
    
  </div>
</div>

  <thead>
    <tr >
      <th>
       
       </th>
      <th class="th-sm">#
      </th>
      <th class="th-sm">Mail
      </th>
      <th class="th-sm">Nombre
      </th>
      <th class="th-sm">Celular
      </th>
      <th class="th-sm">Estado
      </th>
      <th class="th-sm">Pagina Web
      </th>
      <th class="th-sm">Tipo de Suscripcion
      </th>
      <th class="th-sm">Fecha Inicio Suscripcion
      </th>
      <th class="th-sm">Fecha Fin Suscripcion
      </th>
    </tr>
  </thead>
  
  <tbody id='listar_suscripciones'>

</tbody>
        <tfoot>
    <tr>
      <th>
      </th>
      <th>#
      </th>
      <th>Mail
      </th>
      <th>Nombre
      </th>
      <th>Celular
      </th>
      <th>Estado
      </th>
      <th>Pagina Web
      </th>
      <th>Tipo de Suscripcion
      </th>
     
      <th>Fecha Inicio Suscripcion
      </th>
      <th>Fecha Fin Suscripcion
      </th>
    </tr>
  </tfoot>
  
  
  </table>
  </div>
    
   <!-- <table class="table">
      <thead class="thead-dark">
        <tr>
          <th>
                   
                    <button  type="button" id='button' class="btn btn-primary update" data-toggle="modal" data-target-custom="#exampleModalCenter2" >
                    Modificar
                    </button>
                    <br>
                    <br>
                   
                      <button id = 'CargarSus' class="btn btn-secondary " type="button"  aria-haspopup="true" aria-expanded="false">
                        Cargar Suscripcion
                      </button>
                      <br>
                    <br>

                   <br>
                   <div>
                   <input type="checkbox" class="check_all" /> Selecionar Todo
               </div>
                       <br>
                    </th>
          <th scope="col">#</th>
          <th scope="col">Mail </th>
          <th scope="col">Nombre </th>
          <th scope="col">Celular</th>
          <th scope="col">Estado</th>
          <th scope="col">Pagina Web</th>
          <th scope="col">Tipo de Suscripcion</th>
          <th scope="col">Fecha Inicio Suscripcion</th>
          <th scope="col">Fecha Fin Suscripcion</th>
        </tr>-->
        <!--
        **
        ** Listo los usuarios en una tabla con el array que envio desde el controlador listuser
        **
        **
      -->
    <!--</thead>
    <tbody id='listar_suscripciones'>
   
    </tbody>
  </table>-->

  <!--</div>-->
<?php }; ?>