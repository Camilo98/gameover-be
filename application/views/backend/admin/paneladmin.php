<?php
  defined('BASEPATH') OR exit('No direct script access allowed');

?>

      

   

<?php

/* Control para que no puedan ingresar directaemente a la URL sin estar logueado
*/
 if (empty($_SESSION['is_logged_in'])){
    session_destroy();
    redirect();

  }elseif ($_SESSION['rol'] == 'User Admin') {
    $this->session->set_flashdata('mensaje_error', 'No tiene privligios para ingresar');
      redirect(base_url().'listar'); 
      # code...
    }
   
    
  elseif ($_SESSION['rol'] == 'Publicity Admin') {
    $this->session->set_flashdata('mensaje_error', 'No tiene privligios para ingresar');
    redirect(base_url().'galeria');
  }elseif ($_SESSION['rol'] == 'Subscription Admin') {
    $this->session->set_flashdata('mensaje_error', 'No tiene privligios para ingresar');
    redirect(base_url().'gest_suscription');
  }else{
?>



  <div class="container " style="padding: 20px; "  >
    <div style="border-radius: 5px;" 
      class="view view-cascade gradient-card-header default-color-dark narrower py-2 mx-6 mb-5 d-flex justify-content-between align-items-center">

      <div class="d-flex">
       <div class="modal fade" id="Confirmacion3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                      <div class="modal-dialog" role="document">
                      <div class="modal-content">
                      <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Confirmacion</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                     <span aria-hidden="true">&times;</span>
                     </button>
                     </div>
                     <div class="modal-body">
                     <p>Esta seguro que quiere<span id="myText"></span>este usuario?</p>
                     </div>
                     <div class="modal-footer">
                     <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        
                    <button name="Deshabilitar" id="Deshabilitarbtn"  type="button" class="btn btn-primary" data-toggle="modal" data-target="#Confirmacion3">
                    Si, Deshabilitar
                    </button>
                                 
        
                    </div>
                    </div>
                    </div>
                   
                    </div>
                     <div>
                    <button type="button" class="btn btn-outline-white btn-rounded btn-sm px-10 btnloco" data-toggle="modal" data-target="#Confirmacion3"  data-toggle="modal">Deshabilitar
                    </button>
                    </div>
                    
        
    
      <div class="modal fade" id="Confirmacion" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                    <div class="modal-content">
                    <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Confirmacion</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                    </div>
                    <div class="modal-body">
                    <p>Esta seguro que quiere Eliminar este usuario?</p>
                    </div>
                    <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        
                    <button name="Eliminar"  id="Eliminarbtn" type="button" class="btn btn-primary" data-toggle="modal" data-target="#Confirmacion">
                    Si, Eliminar
                    </button>
                                 
        
                    </div>
                    </div>
                    </div>
                    </div>
                    <button type="button"  data-toggle="modal" data-target="#Confirmacion" class=" btn btn-outline-white btn-rounded btn-sm px-10"><span class="oi oi-trash"></span></button>
                  </div>
                      
 
<a href="" class="  white-text mx-3">Administradores del sistema</a>

      <div>
        <button type="button" class="btn btn-outline-white btn-rounded btn-sm px-10 reload">
         <i>
           <span class="oi" data-glyph="reload"></span>
          </i>
        </button>

    
        <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
        <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Perfil Editar</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <div class="modal-body">
                      
         <div class="card" style="width: 30rem;">
         <div class="card-header">
          Datos Personales
         </div>
              
         <ul class="list-group list-group-flush">
         <!-- <a href="<?php echo base_url() . "perfil/editar_foto/?idP=" . $fila->idCuenta; ?>">
         </a> -->
          <li class="list-group-item">
          <label for="inputID">idAdmin</label>
          <input type="text" class="form-control" name="inputID" id="inputID" readonly value=''>
          </li>
          <li class="list-group-item">
          <label for="inputMail">Email</label>
          <input type="text" class="form-control" name="inputMail" id="inputMail" >
          </li>
          <li class="list-group-item">
          <label for="inputUsername">Username</label>
          <input type="text" class="form-control" name="inputUsername" id="inputUsername" >
          </li>
          <li class="list-group-item">
          <label for="inputNombre">Nombre</label>
          <input type="text" class="form-control" name="inputNombre" id="inputNombre" >
          </li>
          <li class="list-group-item">
          <label for="inputApellido">Apellido</label>
          <input type="text" class="form-control" name="inputApellido" id="inputApellido" >
          </li>
             
          <li class="list-group-item">
             <label class="text-dark" for="inputRol">Rol</label>
                    
                    <select  id="inputRol" class="form-control" name="inputRol">
                    <option>General Admin</option>
                    <option>User Admin</option>
                    <option>Publicity Admin</option>
                    <option>Subscription Admin</option>
                  </select>
                  
          
          </li>
          </ul>
          </div>
          <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
          <button class="btn btn-primary update1" type="button" data-dismiss="modal" style="width: 10rem;">Editar</button>
          
          </form>
          </div>
        </div>
      </div>
    </div>
    </div>
          
<button type="button"  data-toggle="modal" data-target-custom="#exampleModalCenter" class=" btn btn-outline-white btn-rounded btn-sm px-10 update">
        <i class="fas fa-pencil-alt mt-2">
        <span class="oi" data-glyph="pencil">
        </span>
        </i>   

        </button>
      </div>
    </div>

     


   <table id="dt-basic-checkbox" class="table table-striped table-bordered" cellspacing="0" width="100%">
       
     <div class="d-flex justify-content-center" >
  <button type="button" class="d-flex btn btn-secondary dropdown-toggle " data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    Mostrar Usuarios:
  </button>
  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
    <a class="dropdown-item" id='enable' href="#">Habilitados</a>
    <a class="dropdown-item" id='disable' href="#">Deshabilitados</a>
   </div>
 
</div>
     
                  
  <thead>
    <tr >
      <th>
       
       </th>
      <th class="th-sm">#
      </th>
      <th class="th-sm">Username
      </th>
      <th class="th-sm">Nombre
      </th>
      <th class="th-sm">Apellido
      </th>
      <th class="th-sm">Correo
      </th>
      <th class="th-sm">Rol
      </th>
      <th class="th-sm">Estado
      </th>
    
    </tr>
  </thead>
  
  <tbody id='listar_usuarios'>

</tbody>
        <tfoot>
    <tr>
      <th>
      </th>
      <th>#
      </th>
      <th>Username
      </th>
      <th>Nombre
      </th>
      <th>Apellido
      </th>
      <th>Correo
      </th>
      <th>Rol
      </th>
      <th>Estado
      </th>
     
    </tr>
  </tfoot>
  
  
  </table>
  </div>

      <!--
      <div class="container">
        <div id="alert">
        
      </div>
            <div class="dropdown">
      <div class="btn-group" style="flex; margin-right: 20px;">
  <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    Mostrar Usuarios:
  </button>
  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
    <a class="dropdown-item" id='enable' href="#">Habilitados</a>
    <a class="dropdown-item" id='disable' href="#">Deshabilitados</a>
   </div>
</div>
</div>



        <div class="card">
         <div class="card-header text-center text-white bg-primary ">
            <h2>Administradores del sistema</h2>
          </div>
          <div class="card-body">
            <blockquote class="blockquote mb-0">
              <?php// echo form_open('padmin/buscar');?>
                <div class="form-group">
                  <input type="text" name="search" value="<?php //echo set_value('cuenta'); ?>" class="form-control" id="search" placeholder="Buscar Usuario">
                </div>
                -->
              <!--</form>-->
            <!--</blockquote>
          </div>
          <div class="card-body">

            <div class="table-responsive">

              <table class="table table-bordered table-responsive">
                <thead class="table">
                  <tr class="table-primary">
                    <th>
                       <div class="modal fade" id="Confirmacion3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                      <div class="modal-dialog" role="document">
                      <div class="modal-content">
                      <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Confirmacion</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                     <span aria-hidden="true">&times;</span>
                     </button>
                     </div>
                     <div class="modal-body">
                     <p>Esta seguro que quiere<span id="myText"></span>este usuario?</p>
                     </div>
                     <div class="modal-footer">
                     <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        
                    <button name="Deshabilitar" id="Deshabilitarbtn"  type="button" class="btn btn-primary" data-toggle="modal" data-target="#Confirmacion3">
                    Si, Deshabilitar
                    </button>
                                 
        
                    </div>
                    </div>
                    </div>
                    </div>
                    <button  type="button" class="btn btn-primary btnloco" data-toggle="modal" data-target="#Confirmacion3">
                    Deshabilitar
                    </button>

                      
                    <br><br>
                    <div class="modal fade" id="Confirmacion" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                    <div class="modal-content">
                    <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Confirmacion</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                    </div>
                    <div class="modal-body">
                    <p>Esta seguro que quiere Eliminar este usuario?</p>
                    </div>
                    <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        
                    <button name="Eliminar"  id="Eliminarbtn" type="button" class="btn btn-primary" data-toggle="modal" data-target="#Confirmacion">
                    Si, Eliminar
                    </button>
                                 
        
                    </div>
                    </div>
                    </div>
                    </div>
                    <button  type="button" class="btn btn-primary" data-toggle="modal" data-target="#Confirmacion" >
                    Eliminar
                    </button>
                    </th>
                    <th scope="col">#</th>
                    <th scope="col">Username</th>
                    <th scope="col">Nombre</th>
                    <th scope="col">Apellido</th>
                    <th scope="col">Correo</th>
                    <th scope="col">Rol</th>
                    <th scope="col">Estado</th>
                    <th scope="col">Modificar</th>
                    </tr>
                    </thead>
                    <tbody id="listar_usuarios">
                  
                    </tbody>
                    </table>
                    </div>
           
                    <a class="btn btn-primary" href="<?php //echo base_url('padmin/AddAdmin');?>">Agregar Usuario</a>
                    </div>

                    </div>
                    </div>
-->

<?php }; ?>