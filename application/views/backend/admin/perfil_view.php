<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="container">

<!-- Modal -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Perfil Editar</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <?php echo form_open('editarPerfil/?id=' .$id); ?>
        
          <?php if ($datoUsuario != NULL) {
            ?>
          <?php foreach($datoUsuario as $fila){ ?>

          <div class="card" style="width: 30rem;">
          <div class="card-header">
            Datos Personales
          </div>
          <
          <ul class="list-group list-group-flush">
              <!-- <a href="<?php echo base_url() . "perfil/editar_foto/?idP=" . $fila->idCuenta; ?>">
                <li class="list-group-item">
                  <img  src="<?php echo base_url() . "assets/uploads/perfil/" . $fila->foto_perfil . ".jpg"; ?>" class="rounded-circle" style="width: 10rem;">
                </li>
              </a> -->
            <li class="list-group-item">
              <label for="inputMail">Email</label>
              <input type="text" class="form-control" name="inputMail" id="inputMail" readonly value= <?php echo $fila->correo;?>>
            </li>
            <li class="list-group-item">
              <label for="inputUsername">Username</label>
              <input type="text" class="form-control" name="inputUsername" id="inputUsername" value= <?php echo $fila->username;?>>
            </li>
            <li class="list-group-item">
              <label for="inputNombre">Nombre</label>
              <input type="text" class="form-control" name="inputNombre" id="inputNombre" value= <?php echo $fila->nombre;?>>
            </li>
            <li class="list-group-item">
              <label for="inputApellido">Apellido</label>
              <input type="text" class="form-control" name="inputApellido" id="inputApellido" value= <?php echo $fila->apellido;?>>
            </li>
            
            <li class="list-group-item">
              <label for="inputRol">Rol</label>
              <input type="text" class="form-control" name="inputRol" id="inputRol" value= <?php echo $fila->rol;?>>
            </li>
          </ul>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button class="btn btn-primary" type="submit" style="width: 10rem;">Editar</button>
        <?php } } ?>
      </form>
      </div>
    </div>
  </div>
</div>
</div>
<!-- Fin Modal -->


    <?php
    if ($datoUsuario != NULL)
    {
      foreach($datoUsuario as $fila)
      {
    ?>

    <div class="card" style="width: 30rem;">
    <div class="card-header">
      <?php echo("$fila->username $fila->apellido")  ?>
    </div>
    
    <ul class="list-group list-group-flush">
      <!-- <li class="list-group-item">
        <img  src="<?php echo base_url("assets/uploads/perfil/$fila->foto_perfil.jpg"); ?>" class="rounded-circle" style="width: 10rem;">
      </li> -->
      <li class="list-group-item">
        <label for="inputMail">Email</label>
        <input type="text" class="form-control" name="inputMail" id="inputMail" readonly value= <?php echo $fila->correo;?>>
      </li>
      <li class="list-group-item">
        <label for="inputNombre">Nombre</label>
        <input type="text" class="form-control" name="inputNombre" id="inputNombre" readonly value= <?php echo $fila->nombre;?>>
      </li>
      <li class="list-group-item">
        <label for="inputApellido">Apellido</label>
        <input type="text" class="form-control" name="inputApellido" id="inputApellido" readonly value= <?php echo $fila->apellido;?>>
      </li>
      
      <li class="list-group-item">
        <label for="inputCel">Username</label>
        <input type="text" class="form-control" name="inputCel" id="inputCel" readonly value= <?php echo $fila->username;?>>
      </li>
      <li class="list-group-item">
        <label for="inputCel">Rol</label>
        <input type="text" class="form-control" name="inputCel" id="inputCel" readonly value= <?php echo $fila->rol;?>>
      </li>
    
    </ul>

    <!-- Button trigger modal -->
<!--<?php //if($_SESSION['userid'] == $fila->idAdmin ) {?>-->
  <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter" style="width: 20rem;">
    Editar datos
  </button>

<?php } }  ?> 

</div>
</div>
