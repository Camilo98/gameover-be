<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if (isset($_SESSION['is_logged_in'])) {
 
?>

   <div class="login-box">
  
  <div class="login-logo">
    <a><b>Tu sesion quedo activa!</b> inicia sesion para continuar o cambia de usuario</a>
  </div>
  <!-- /.login-logo -->

  <div class="login-box-body">
    <p class="login-box-msg">Iniciar Sesion</p>

    <form action="<?php echo base_url(); ?>signin" method="post">
      <div class="form-group has-feedback">
        <input value="<?php echo set_value('username'); ?>" type="text" class="form-control" id="username" name="username" placeholder="Ingrese su Usuario">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control" name="pass" id="pass" placeholder="Ingrese su Contraseña">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">
      
  
  
        <!-- /-->
        <div class="col col-xs-8">
          <button type="submit" class="btn btn-primary btn-block btn-flat col-sm">Iniciar</button>
        </div>
        
        <div class="col col-xs-8">
             <a class="nav-link" href="<?php base_url();?>logout?>">Cambiar de Usuario</a>
     
         
        </div>
        <!-- /.col -->
      </div>
    </form>
    
      
        
      </div>
  <!-- /.login-box-body -->
</div>
<?php

    }else{

     
?>

 

<div class="login-box">
  
  <div class="login-logo">
    <a><b>GAME</b>OVER</a>
  </div>
  <!-- /.login-logo -->

  <div class="login-box-body">
    <p class="login-box-msg">Iniciar Sesion</p>

    <form action="<?php echo base_url(); ?>signin" method="post">
      <div class="form-group has-feedback">
        <input value="<?php echo set_value('username'); ?>" type="text" class="form-control" id="username" name="username" placeholder="Ingrese su Usuario">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control" name="pass" id="pass" placeholder="Ingrese su Contraseña">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">
       <div class="col-sm">
              
    </div>
  
  
        <!-- /-->
        <div class="col col-xs-8">
          <button type="submit" class="btn btn-primary btn-block btn-flat col-sm">Sign In</button>
        </div>
          <div class="">
             
    </div>

        <!-- /.col -->
      </div>
    </form>
    <div  class="col-xs-8">
      <br>
      
        </div>
      
        
      </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<!-- jQuery 2.2.0 -->
<script src="<?php echo base_url();?>assets/plugins/jQuery/jQuery-2.2.0.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="<?php echo base_url();?>assets/bootstrap/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="<?php echo base_url();?>assets/plugins/iCheck/icheck.min.js"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
</script>

<?php } ?>
