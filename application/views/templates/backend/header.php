<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" >GO!</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
 


     

    <!-- <form class="form-inline my-2 my-lg-0">
      <input class="form-control mr-sm-2" type="search" placeholder="Buscar usuario o grupos" aria-label="Search">
      <button class="btn btn-outline-dark my-2 my-sm-0" type="submit">Buscar</button>
    </form>

    -->

    <ul class="navbar-nav navbar-rigth">
      <?php  if (!empty($_SESSION['is_logged_in']) && $_SESSION['rol'] == 'User Admin'):?>
      <!-- Boton par alistar usuarios-->
      <li class="nav-item">
        <a class="nav-link" href="<?php echo base_url("listar");?>">Usuarios</a>
      </li>
      <?php endif; ?>
      <?php  if (!empty($_SESSION['is_logged_in']) && $_SESSION['rol'] == 'Pubicity Admin'):?>
      <!-- Boton Para ir a Galeria -->
      <li class="nav-item">
        <a class="nav-link" href="<?php echo base_url("galeria");?>">Galería</a>
      </li>
      <?php endif; ?>
    

      <?php  if (!empty($_SESSION['is_logged_in'])):?>

      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <?php echo $_SESSION['nomuser'] ?>
        </a>
        <!--
        *
        **menu desplegable
        *
        -->
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                 
          <div class="dropdown-divider"></div>
          
          <a class="dropdown-item" href="<?php base_url();?>logout">Salir</a>
        </div>
      </li>
      <?php endif; ?>
    </ul>
      <!--
      * habilitado si el usuario no está logueado
      -->
   

  </div>

</nav>
  <div id="alert">
        
      </div>
<?php
if($this->session->flashdata('mensaje_error') !=NULL){
  echo "<div class='alert alert-danger' role='alert'>";
  echo  $this->session->flashdata('mensaje_error');
  echo "</div>";
}
 if($this->session->flashdata('mensaje_success') !=NULL){
   echo "<div class='alert alert-success' role='alert'>";
   echo  $this->session->flashdata('mensaje_success');
   echo "</div>";
}
 ?>

