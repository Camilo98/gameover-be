<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Administrador{

  	private $id_admin;
  	private $rol;
  	private $username;
  	private $nombre;
    private $apellido;
    private $correo;
    private $pass;
    private $estado;


    public function __construct()
    {

    }

   		 // SETTERS //


     function set_id_admin($id)
    {
    	$this->id_admin = $id;
    }

     function set_rol($rol)
    {
    	$this->rol = $rol;
    }

     function set_username($username)
    {
    	$this->username = $username;
    }

     function set_nombre($nombre)
    {
    	$this->nombre = $nombre;
    }

     function set_apellido($apellido)
    {
    	$this->apellido = $apellido;
    }

     function set_correo($correo)
    {
    	$this->correo = $correo;
    }

     function set_pass($pass)
    {
    	$this->pass = $pass;
    }

     function set_estado($estado)
    {
    	$this->estado = $estado;
    }
    

    	// GETTERS //


    function get_id_admin()
    {
    	return $this->id_admin;
    }

    function get_rol()
    {
    	return $this->rol;
    }

    function get_username()
    {
    	return $this->username;
    }

    function get_nombre()
    {
    	return $this->nombre;
    }

    function get_apellido()
    {
    	return $this->apellido;
    }

    function get_correo()
    {
    	return $this->correo;
    }

    function get_pass()
    {
    	return $this->pass;
    }

    function get_estado()
    {
    	$this->estado;
    }

      //--verifica existencia--//
  public static function existe_cuenta($mail)
  {
    $CI = &get_instance();
    $CI->load->model('Auth_model');
    $log = $CI->Auth_model->getUser($mail);
    if (!$log)
    {
        return FALSE;
    }
      else
      {
        return TRUE;
      }
  }

    public static function verif_login($username,$pass)
  {
    $CI = &get_instance();
    $CI->load->model('Auth_model');
    $log = $CI->Auth_model->getUser($username);
    if($log->pass != sha1(md5($pass)))
    {
      return FALSE;
    }
    else
    {
      //die(print_r($user->nomUser, TRUE));
      if ($log->estado == 0)
      {
          //--usuario BLOQUEADO--//
          $userbe = "ERROR_001";
          //die(print_r($cuenta, TRUE));
      }
      else
        {
          $id=$log->id_admin;
          $userbe = new General_admin();
          $userbe->set_id_admin($id);
          $userbe->set_nombre($log->nombre);
          $userbe->set_rol($log->rol);
          
        }
        return $userbe;
      }
    }

      function verif_user($userbe)
    {
    $email=$userbe->get_correo();
    $existe=Administrador::existe_cuenta($email);
    if ($existe)
    {
      //--si correo está en uso envia ERROR_002--//
      $usuario_creado = "ERROR_002";
    }else
      {

               
            $CI = &get_instance();
            $CI->load->model('mAdmin');
            $param['nombre']=$userbe->get_nombre();
            $param['apellido']=$userbe->get_apellido();
            $param['correo']=$userbe->get_correo();
            $param['pass']=$userbe->get_pass();
            $param['username']=$userbe->get_username();
            $param['rol']=$userbe->get_rol();
            $param['estado']= 1 ;
            
            
            $CI->mAdmin->guardar($param);
            //die(print_r($usuario,true));
            $usuario_creado = $userbe->get_username();
          
      }
          return $usuario_creado;
     }

 

  
 }