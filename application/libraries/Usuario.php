<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once('Cuenta.php');
class Usuario extends Cuenta
{
  function __construct()
  {
     parent::__construct();
  }

  private $nombre;
  private $apellido;
  private $fechaNac;
  private $genero;

  //--getters--//
  function get_nombre()
  {
    return $this->nombre;
  }
  function get_apellido()
  {
    return $this->apellido;
  }
  function get_fecha_nac()
  {
    return $this->fechaNac;
  }
  function get_genero()
  {
    return $this->genero;
  }

  //--setters--//
  function set_nombre($nombre)
  {
    $this->nombre = $nombre;
  }
  function set_apellido($apellido)
  {
    $this->apellido = $apellido;
  }
  function set_fecha_nac($fechaNac)
  {
    $this->fechaNac = $fechaNac;
  }
  function set_genero($genero)
  {
    $this->genero = $genero;
  }

}