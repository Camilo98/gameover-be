<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once('Administrador.php');
class General_admin extends Administrador
{
	public function __construct()
	{
		
		
	}

	function eliminar_admin($checkbox_value)
	{
		$CI = &get_instance();
		$CI->load->model('mAdmin');
		$opcion = $CI->mAdmin->Delete_admin($checkbox_value);

             
              return $opcion;
	}

	function deshabilitar_admin($checkbox_value)
	{
		$CI = &get_instance();
		$CI->load->model('mAdmin');
		$opcion = $CI->mAdmin->Disable_admin($checkbox_value);
		//$opcion = $checkbox_value;

             
              return $opcion;

	}

	function habilitar_admin($checkbox_value)
	{
		$CI = &get_instance();
		$CI->load->model('mAdmin');
		$opcion = $CI->mAdmin->Enable_admin($checkbox_value);

             
              return $opcion;

	}


	function modificar_admin($param)
	{
		$CI = &get_instance();
		$CI->load->model('mAdmin');
		$CI->mAdmin->Modificar1($param);


	}

	function buscar_admin($name)
	{
		$CI = &get_instance();
		$CI->load->model('mAdmin');
		$listadmin = $CI->mAdmin->Buscar($name);

		return $listadmin;


	}

}