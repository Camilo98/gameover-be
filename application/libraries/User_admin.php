<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once('Administrador.php');
class User_admin extends Administrador
{
	public function __construct()
	{
		
	}

	function deshabilitar_admin($checkbox_value)
	{
		$CI = &get_instance();
		$CI->load->model('getuser_model');
		$opcion = $CI->getuser_model->disable_admin($checkbox_value);
		
             
              return $opcion;

	}


	function habilitar_admin($checkbox_value)
	{
		$CI = &get_instance();
		$CI->load->model("getuser_model");
		$opcion = $CI->getuser_model->habilitar_admin($checkbox_value);


		return $opcion;
	}


	function eliminar_admin($checkbox_value)
	{
		$CI = &get_instance();
		$CI->load->model("getuser_model");
		$opcion = $CI->getuser_model->eliminar_admin($checkbox_value);

		return $opcion;
	}

	function modificar_user($param)
	{
		$CI = &get_instance();
		$CI->load->model("getuser_model");
		if ($param[PagWeb]) {
			$opcion = $CI->getuser_model->Modificar2($param);
		}else{
		$opcion = $CI->getuser_model->Modificar1($param);
		}
		return $opcion;
	}

		function buscar_admin($name)
	{
		$CI = &get_instance();
		$CI->load->model('getuser_model');
		$listadmin = $CI->getuser_model->Buscar($name);

		return $listadmin;


	}

		function get_tipouser($id)
	{
		$CI = &get_instance();
		$CI->load->model('getuser_model');
		$tipouser = $CI->getuser_model->get_tipouser($id);

		return $tipouser;


	}

		function get_muro($id, $tipo)
	{
		$CI = &get_instance();
		$CI->load->model('getuser_model');
		$tipouser = $CI->getuser_model->get_muro($id, $tipo);

		return $tipouser;


	}

		function get_user($id, $tipo)
	{
		$CI = &get_instance();
		$CI->load->model('getuser_model');
		$tipouser = $CI->getuser_model->get_user($id, $tipo);

		return $tipouser;


	}
}