<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Publicacion{
  private $id_publicacion;
  private $tipo;
  private $cont_post;
  private $link_video;
  private $nom_img;
  private $nom_album;
  private $date_post;
  private $estado;


  public function __construct()
  {

  }

  

  //-- getters --//
  function get_id_post()
  {
    return $this->id_post;
  }
  function get_tipo()
  {
    return $this->type_post;
  }
  function get_cont_post()
  {
    return $this->cont_post;
  }
  function get_link_video()
  {
    return $this->link_video;
  }
  function get_nom_img()
  {
    return $this->nom_img;
  }
  function get_nom_album()
  {
    return $this->nom_album;
  }
  function get_date_post()
  {
    return $this->date_post;
  }
  function get_estado()
  {
    return $this->estado;
  }

  //-- setters --//
  function set_id_post($id_post)
  {
    $this->id_post = $id_post;
  }
  function set_type_post($type_post)
  {
    $this->type_post = $type_post;
  }
  function set_cont_post($cont_post)
  {
    $this->cont_post = $cont_post;
  }
  function set_link_video($link_video)
  {
    $this->link_video = $link_video;
  }
  function set_nom_img($nom_img)
  {
    $this->nom_img = $nom_img;
  }
  function set_nom_album($nom_album)
  {
    $this->nom_album = $nom_album;
  }
  function set_date_post($date_post)
  {
    $this->date_post = $date_post;
  }
  function set_estado($estado)
  {
    $this->estado = $estado;
  }
}