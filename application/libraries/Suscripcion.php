<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Suscripcion{
  private $id_suscrip;
  private $id_cuenta;
  private $inicio;
  private $fin;
  private $estado;


  public function __construct()
  {

  }

  //getters
  function get_id_suscrip()
  {
    return $this->id_suscrip;
  }
  function get_id_cuenta()
  {
    return $this->id_cuenta;
  }
  function get_inicio()
  {
    return $this->inicio;
  }
  function get_fin()
  {
    return $this->fin;
  }
  function get_estado()
  {
    return $this->estado;
  }

  //setters
  function set_id_suscrip($id_suscrip)
  {
    $this->id_suscrip = $id_suscrip;
  }
  function set_id_cuenta($id_cuenta)
  {
    $this->id_cuenta = $id_cuenta;
  }
  function set_inicio($inicio)
  {
    $this->inicio = $inicio;
  }
  function set_fin($fin)
  {
    $this->fin = $fin;
  }
  function set_estado($estado)
  {
    $this->estado = $estado;
  }
}
