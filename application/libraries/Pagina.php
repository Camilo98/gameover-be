<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once('Cuenta.php');
class Pagina extends Cuenta
{
  function __construct()
  {
     parent::__construct();

  }

  private $nombre;
  private $inicio;
  private $web;

  //-- getters --//
  function get_nombre()
  {
    return $this->nombre;
  }
  function get_inicio()
  {
    return $this->fecha_inicio;
  }
  function get_web()
  {
    return $this->web;
  }

  //-- setters --//
  function set_nombre($nombre)
  {
    $this->nombre = $nombre;
  }

  function set_inicio($fecha_inicio)
  {
    $this->fecha_inicio = $fecha_inicio;
  }
  function set_web($web)
  {
    $this->web = $web;
  }
}