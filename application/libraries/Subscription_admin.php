<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once('Administrador.php');
class Subscription_admin extends Administrador
{
	function __construct()
	{
		parent::__construct();
	}

	function fetch_suscriptions($id)
	{
		$CI = &get_instance();
		$CI->load->modal('getuser_model');
		$opcion = $CI->getuser_model->fetch_suscriptions($id);
	}

	function listar_suscripciones($estado)
	{
		$CI = &get_instance();
		$CI->load->model('getuser_model');
		$opcion = $CI->getuser_model->getRecordsPaginaSuscriptas($estado);
		
             
              return $opcion;

	}

	function listar_suscripciones_segunestado($param)
	{
		$CI = &get_instance();
		$CI->load->model('getuser_model');
		$opcion = $CI->getuser_model->getRecordsPaginasegunestado($param);
		
             
              return $opcion;

	}

	function updateTipoSuscripcion($param1)
	{
		$CI = &get_instance();
		$CI->load->model('getuser_model');
		$opcion = $CI->getuser_model->updateSuscripcion($param1);
             
              return $opcion;

	}

	function updateSuscripcion($param1)
	{
		$CI = &get_instance();
		$CI->load->model('getuser_model');
		$opcion = $CI->getuser_model->Modificar3($param1);
		
		return $opcion;

		//var_dump($param1);
	}

	function registrar_notificacion($param)
	{
		$CI = &get_instance();
		$CI->load->model('getuser_model');
		$opcion = $CI->getuser_model->registrar_notificacion($param);
		
             
              return $opcion;

	}
}