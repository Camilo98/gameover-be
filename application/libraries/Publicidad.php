<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Publicidad{

	private $id_publicidad;
	private $nombre;
	private $contpost;
	private $imagen;
	private $tipo;


	// SETTERS //

	function set_id_publicidad($id)
	{
		$this->id_publicidad = $id;
	}

	function set_nombre($nombre)
	{
		$this->nombre = $nombre;
	}

	function set_contpost($contpost)
	{
		$this->contpost = $contpost;
	}

	function set_imagen($imagen)
	{
		$this->imagen = $imagen;
	}

	function set_tipo($tipo)
	{
		$this->tipo = $tipo;
	}

    //  GETTERS //

    function get_id_publicidad()
    {
    	return $this->id_publicidad;
    }

    function get_nombre()
    {
    	return $this->nombre;
    }

    function get_contpost()
    {
    	return $this->contpost;
    }

    function get_imagen()
    {
    	return $this->imagen;
    }

    function get_tipo()
    {
    	return $this->tipo;
    }

}