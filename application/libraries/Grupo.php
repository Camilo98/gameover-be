<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Grupo{

	private $id_grupo;
	private $nom_grupo;
	private $fecha_inicio;
	private $fecha_fin;


	// SETTERS //

	function set_id_grupo($id)
	{
		$this->id_grupo = $id;
	}

	function set_nom_grupo($nom_grupo)
	{
		$this->nom_grupo = $nom_grupo;
	}

	function set_fecha_inicio($fecha_inicio)
	{
		$this->fecha_inicio = $fecha_inicio;
	}

	function set_fecha_fin($fecha_fin)
	{
		$this->fecha_fin = $fecha_fin;
	}

	// GETERS //

	function get_id_grupo()
	{
		return $id_grupo;
	}

	function get_nom_grupo()
	{
		return $nom_grupo;
	}

	function get_fecha_inicio()
	{
		return $fecha_inicio;
	}

	function get_fecha_fin()
	{
		return $fecha_fin;
	}
}