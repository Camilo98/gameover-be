<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cuenta
{
  private $id;
  private $email;
  private $pass;
  private $cel;
  private $foto;
  private $estado;

public function __construct()
  {

  }

  //--getters--//
  function get_id()
  {
    return $this->id;
  }
  function get_email()
  {
    return $this->email;
  }
  function get_pass()
  {
    return $this->pass;
  }
  function get_cel()
  {
    return $this->cel;
  }
  function get_foto()
  {
    return $this->foto;
  }
  function get_estado()
  {
    return $this->estado;
  }

  //--setters--//
  function set_id($id)
  {
    $this->id = $id;
  }
  function set_email($email)
  {
    $this->email = $email;
  }
  function set_pass($pass)
  {
    $this->pass = $pass;
  }
  function set_cel($cel)
  {
    $this->cel = $cel;
  }
  function set_foto($foto)
  {
    $this->foto = $foto;
  }
  function set_estado($estado)
  {
    $this->estado = $estado;
  }



}