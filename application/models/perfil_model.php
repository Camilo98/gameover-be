<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class perfil_model extends CI_Model{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
  }

  function dataUser($id)
  {
      $this->db->select('*');
      $this->db->from('userbe');
      $this->db->where('userbe.idAdmin', $id);
      
      $dataUser = $this->db->get();
      return $dataUser->result();

  }

  function updateUser($data)
  {

     $id = $data['idAdmin'];
    
    $data = array(
      'correo' => $data['correo'],
      'username' => $data['username'],
      'nombre' => $data['nombre'],
      'apellido' => $data['apellido'],
      'rol' => $data['rol']

  );
    $this->db->where('idAdmin', $id);
    $this->db->update('userbe',$data);


      /*$dataUsuario = array(
        'nomUser' => $data['nomUser'],
        'apeUser' => $data['apeUser'],
        'fecNac' => $data['fecNac']
      );
      $this->db->where('idCuenta_fk',$_SESSION['userid']);
      $this->db->update('usuario',$dataUsuario);*/
  }
}
