<?php
class Reg_model extends CI_Model {
  public function __construct()
  {
    parent::__construct();
  }

  public function guardar($param)
  {
    $campos = array(
      'nomUser'=>$param['nomUser'],
      'apeUser'=>$param['apeUser'],
      'mailUser'=>$param['mailUser'],
      'passUser'=>sha1(md5($param['passUser'])),
      'celUser'=>$param['celUser'],
      'fecNac'=>$param['fecNac']
    );
    $this->db->INSERT('cuenta', $campos);
  }
}
?>
