<?php
/**
 *
 */
class getuser_model extends CI_Model
{
  public function __construct()
  {
    
  parent::__construct();

  }

  public function getRecords($estado = '')
  {

     
    $this->db->select('usuario.nomUser, usuario.apeUser, cuenta.id_cuenta, cuenta.cel_cuenta, cuenta.mail_cuenta, usuario.fec_nac, cuenta.estado, usuario.sex_user');
    $this->db->from('cuenta, usuario');
    $this->db->where('cuenta.estado', $estado);
     $this->db->where('cuenta.id_cuenta = usuario.id_cuenta_fk');
   
    $sql=$this->db->get();

     /*if($sql->num_rows()>0){
       foreach ($sql->result() as $fila):
         $data[]=$fila;
       endforeach;
     }*/
     
    
     //die(print_r($data));
     
   return $sql->result();
   //return $data;
   }
public function getRecordsPagina($estado = '')
  {

    $this->db->from('cuenta, pagina');
     $this->db->where('cuenta.estado', $estado);
    $this->db->where('cuenta.id_cuenta = pagina.id_cuenta_fk');
    
   $sql=$this->db->get();

    /* if($sql->num_rows()>0){
       foreach ($sql->result() as $fila):
         $data2[]=$fila;
       endforeach;
     }*/
     return $sql->result();
    // return $data2;
   }
public function getRecordsPaginaSuscriptas($estado = '')
  {
    $permission = array('1', '2', '3');


    $this->db->from('cuenta, pagina, suscripcion');
    $this->db->where('cuenta.id_cuenta = pagina.id_cuenta_fk');
    $this->db->where('suscripcion.id_cuenta_fk = cuenta.id_cuenta');
     $this->db->where('cuenta.estado', $estado);
     $this->db->where_in('suscripcion.estado', $permission);
     //$this->db->where_in('suscripcion.estado', $permission);
    
    $sql=$this->db->get();

    /* if($sql->num_rows()>0){
       foreach ($sql->result() as $fila):
         $data2[]=$fila;
       endforeach;
     }*/
     return $sql->result();
    // return $data2;
   }

public function updateSuscripcion($param)
  {


     $data = array(
      'type' => $param['type'],
      'fecha_fin' => $param['fecha_fin'],
      'estado' => '0',
      'fecha_ini' => $param['fecha_ini']

  ); 
    $id = $param['idsus'];



      $this->db->where('id_suscrip', $id);
      $this->db->update('suscripcion',$data);
     /* $id = $param['id'];
      $ffin = $param['fechaFin'];
      $tipo = $param['type'];
      $this->db->set('type', $tipo, FALSE);
      $this->db->where('idCuenta_fk', $id);
      $this->db->update('suscripcion');

      $this->db->set('fechaFin', $ffin, FALSE);
      $this->db->where('idCuenta_fk', $id);
      $this->db->update('suscripcion');*/

      return '2';
     // return $data;
      
  }

    public function fetch_suscription($id) {

    $id = $id;
    $this->db->select('*');
    $this->db->from('suscripcion');
    $this->db->where('suscripcion.id_suscrip', $id);
    
      $result1 = $this->db->get();
      return $result1->result();

}

  public function fetchuser($id) {

    $id1 = $id;
    $this->db->select('*');
      $this->db->from('cuenta, usuario');
    $this->db->where('cuenta.id_cuenta = usuario.id_cuenta_fk');
    $this->db->where('cuenta.id_cuenta', $id1);
    $this->db->where('usuario.id_cuenta_fk ', $id1);
      $result1 = $this->db->get();
      return $result1->result();

}


 public function fetchpage($id) {

    $id1 = $id;
    $this->db->select('*');
      $this->db->from('cuenta, pagina');
    $this->db->where('cuenta.id_cuenta = pagina.id_cuenta_fk');
    $this->db->where('cuenta.id_cuenta', $id1);
    $this->db->where('pagina.id_cuenta_fk ', $id1);
      $result1 = $this->db->get();
      return $result1->result();

}







public function Modificar1($param){


 $data = array(
      'mail_cuenta' => $param['correo'],
      'cel_cuenta' => $param['Cel']
      

  ); 
    $id = $param['id'];



      $this->db->where('id_cuenta', $id);
      $this->db->update('cuenta',$data);
      
      $data2 =array(

        'nom_user' => $param['username'],
        'ape_user' => $param['apellido'],
        'fec_nac' => $param['FecNac'],
        'sex_user' => $param['Sex']


         );
       $this->db->where('id_cuenta_fk', $id);
      $this->db->update('usuario',$data2);

      return '2';
  }


//------------------------------------------------------------------------------
  
  //-----------------------------

  public function Modificar3($param){


 $data = array(
      'estado' => $param['Estado'],
      'fecha_ini' => $param['FecIni'],
      'fecha_fin' => $param['FecFin'],
      'type' => $param['Type']
      

  ); 
    $id = $param['IdSuscrip'];



      $this->db->where('id_suscrip', $id);
      $this->db->update('suscripcion',$data);
      
      
      return '2';
      ///return $param;
      //var_dump($param);
  }

  //---------------------------------------------

  public function get_muro($id1, $tipo){

    //if ($_SESSION['is_page']) {
    /*  $this->db->from('comparte')->where('comparte.idCuenta_fk',$id_user);
      $this->db->join('pagina','pagina.idCuenta_fk = comparte.idCuenta_fk');
      $this->db->join('publicacion','publicacion.idpublicacion = comparte.idPublicacion_fk');
      $query = $this->db->order_by('datePost','DESC')->get();*/
      //return $query->result();
   // }else {
      $multipleWhere = ['comparte.id_cuenta_fk' => $id1, 'publicacion.estado' => '1'];

     

      if (in_array("1", $tipo)) {
        # code...
     
      $this->db->from('comparte')->where($multipleWhere);
  
      $this->db->join('usuario','usuario.id_cuenta_fk = comparte.id_cuenta_fk');
      $this->db->join('publicacion','publicacion.id_publicacion = comparte.id_publicacion_fk');
      $query = $this->db->order_by('datePost','DESC')->get();
      return $query->result();
      
       }elseif (in_array("2", $tipo)) {
         
         $this->db->from('comparte')->where($multipleWhere);
       
      $this->db->join('pagina','pagina.id_cuenta_fk = comparte.id_cuenta_fk');
      $this->db->join('publicacion','publicacion.id_publicacion = comparte.id_publicacion_fk');

      $query = $this->db->order_by('datePost','DESC')->get();
      return $query->result();
       }

    //}

  }

  public function get_tipouser($id){
    $this->db->select('tipo');
    $this->db->from('cuenta')->where('id_cuenta', $id);
    $result1 = $this->db->get();
      return $result1->row();
  }
   
public function get_user($id, $tipo){

  if (in_array('1', $tipo)) {
   $this->db->select('usuario.nom_user');
   $this->db->from('usuario')->where('usuario.id_cuenta_fk', $id);
   $result1 = $this->db->get();
      return $result1->row();
  }elseif (in_array('2', $tipo)) {
    $this->db->select('pagina.nombre');
     $this->db->from('pagina')->where('pagina.id_cuenta_fk', $id);
   $result1 = $this->db->get();
      return $result1->row();
    
  }
  
}

 public function disable_admin($checkbox_value)
    {
      for($count = 0; $count < count($checkbox_value); $count++)
              {
                  $id = $checkbox_value[$count];
                  $this->db->set('estado', '0', FALSE);
                  $this->db->where('id_cuenta', $id);
                  $this->db->update('cuenta'); 

           
          
              }
                  
       return '2';
    }


  public function habilitar_admin($checkbox_value)
  {
      for($count = 0; $count < count($checkbox_value); $count++)
              {
                  $id = $checkbox_value[$count];
                  $this->db->set('estado', '1', FALSE);
                  $this->db->where('id_cuenta', $id);
                  $this->db->update('cuenta'); 

          
              }

  }

    public function eliminar_admin($checkbox_value)
  {
      for($count = 0; $count < count($checkbox_value); $count++)
              {
                  $id = $checkbox_value[$count];
                  
                    $this->db->where('id_cuenta', $id);
                    $this->db->delete('cuenta'); 
              }

         return '2';

  }

  public function Buscar($name)
  {
   // die(print_r($estado));

    $this->db->select('*');
    $this->db->from('userbe');
    
    $this->db->or_like('username', $name, 'after');// Produces: WHERE title LIKE 'match%'
    
    $this->db->or_like('nombre', $name, 'after');
    $this->db->or_like('apellido', $name, 'after');
    $this->db->or_like('correo', $name, 'after'); 
    $this->db->or_like('rol', $name, 'after'); 
   
   
    
    $listuser = $this->db->get();

    return $listuser->result();
 
  }


  public function getRecordsPaginasegunestado($param = '')
  {
    
    $estado = $param[0];
    $estadosus = $param[1];
    $today = $param[2];
    $mostrar = $param[3];

    if ($estadosus == 0) {
     if ($mostrar != 0){
      $this->db->from('cuenta, pagina');
      // $this->db->where('cuenta.estado', $estado);
      $this->db->where('cuenta.id_cuenta = pagina.id_cuenta_fk');
      $this->db->join('suscripcion','suscripcion.id_cuenta_fk = cuenta.id_cuenta');
      $this->db->where('suscripcion.type != 0');
      $this->db->where('suscripcion.fecha_fin >', $today);
      $sql=$this->db->get();
      
	
	}else{
	$this->db->from('cuenta, pagina');
      // $this->db->where('cuenta.estado', $estado);
      $this->db->where('cuenta.id_cuenta = pagina.id_cuenta_fk');
      $this->db->join('suscripcion','suscripcion.id_cuenta_fk = cuenta.id_cuenta');
      $this->db->where('suscripcion.type != 0');
      $this->db->where('suscripcion.fecha_fin >', $today);
	$this->db->where('suscripcion.notificacion = 0');
      $sql=$this->db->get();
      
      }


    }elseif ($estadosus == 2) {

      $this->db->from('cuenta');
      // $this->db->where('cuenta.estado', $estado);
      //$this->db->where('cuenta.id_cuenta = pagina.id_cuenta_fk');
      $this->db->join('pagina','pagina.id_cuenta_fk = cuenta.id_cuenta');
      $this->db->join('suscripcion','suscripcion.id_cuenta_fk = cuenta.id_cuenta');
      $this->db->where('suscripcion.type != 0');
      $this->db->where('suscripcion.fecha_fin <=', $today);
      $sql=$this->db->get();
      
    }elseif ($estadosus == 3){

      

$this->db->select('id_suscrip')->from('suscripcion');
$where = "estado = '0' AND type = '0'";
$this->db->where($where);


$subQuery =  $this->db->get_compiled_select();
 
// Main Query
$this->db->select('*');
 $this->db->from('cuenta');
$this->db->join('suscripcion','suscripcion.id_cuenta_fk = cuenta.id_cuenta');
$this->db->join('pagina','pagina.id_cuenta_fk = cuenta.id_cuenta');
 $this->db->where("suscripcion.id_suscrip NOT IN ($subQuery)");
 $sql=$this->db->get();
         



     // $sql=$this->db->get();

    }
    

  
     return $sql->result();
    // return $data2;
   }

   public function registrar_notificacion($param = ''){


                  $mail = $param[0];
                   $today = $param[1];

                   $this->db->select('id_cuenta')->from('cuenta');
                   
                    $this->db->where("cuenta.mail_cuenta =", $mail);

                    $subQuery =  $this->db->get_compiled_select();

                  $this->db->set('notificacion', '1', FALSE);
            $this->db->where("suscripcion.id_cuenta_fk IN ($subQuery)");
                  
                   $this->db->where('suscripcion.type != 0');
                 $this->db->where('suscripcion.fecha_fin >', $today);
                  $this->db->update('suscripcion'); 

   }

}
