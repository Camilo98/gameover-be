<?php
/**
 *
 */
class Post extends CI_Model
{

  function __construct()
  {
    parent::__construct();

  }
  /*Recoge los post y los ordena en forma descendente por fecPost
  */
  public function get()
  {
    return $this->db->order_by('fecPost','DESC')->get("posts")->result();
  }
/*
*Agrega un nuevo post a la tabla 
*/
  public function addpost($param)
  {
      $campos = array(
        'title'=>$param['title'],
        'content'=>$param['content'],
        'userId'=>$param['userId']
      );
      $this->db->INSERT('posts', $campos);
  }
}

?>
