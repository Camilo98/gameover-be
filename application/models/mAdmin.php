<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class mAdmin extends CI_Model{

  public function __construct()
  {
    parent::__construct();
   
  }


   public function listar($estado = '')
  {
    
    $this->db->select('*');
    $this->db->from('userbe');
    $this->db->not_like('rol', 'Superusuario', 'after');
      $this->db->where('userbe.estado', $estado);
    $listuser = $this->db->get();


    return $listuser->result();//result() devuelve un array de objetos, row() devuelve la fila
  }


  public function Buscar($name)
  {
   // die(print_r($estado));

    $this->db->select('*');
    $this->db->from('userbe');
    
    $this->db->or_like('username', $name, 'after');// Produces: WHERE title LIKE 'match%'
    
    $this->db->or_like('nombre', $name, 'after');
    $this->db->or_like('apellido', $name, 'after');
    $this->db->or_like('correo', $name, 'after'); 
    $this->db->or_like('rol', $name, 'after'); 
   
   
    
    $listuser = $this->db->get();

    return $listuser->result();
    //result() devuelve un array de objetos, row() devuelve la fila
    /*$this->db->select('*');
    $this->db->from('userbe');
    $this->db->like('username', $name, 'after');
    $this->db->or_like('nombre', $name, 'after');
    $this->db->or_like('apellido', $name, 'after');
    $this->db->or_like('correo', $name, 'after'); 
    $this->db->or_like('rol', $name, 'after');*/
  
    //echo $estado;
    
    //echo $prueba;
    
    //var_dump($prueba);
  }

  public function Modificar1($param){


 $data = array(
      'correo' => $param['correo'],
      'username' => $param['username'],
      'nombre' => $param['nombre'],
      'apellido' => $param['apellido'],
      'rol' => $param['rol']

  ); 
    $id = $param['id'];



      $this->db->where('id_admin', $id);
      $this->db->update('userbe',$data);
      
  }

  public function guardar($param)
  {

      $campos = array(
      'correo'=>$param['correo'],
      'pass'=>sha1(md5($param['pass'])),
      'rol'=>$param['rol'],
      'nombre'=>$param['nombre'],
      'apellido'=>$param['apellido'],
      'username'=>$param['username'],
      'estado'=>$param['estado'],
    );
    $this->db->INSERT('userbe', $campos);
    


    }

  


  /*  public function verificar($username){

 $name = $username;
 $this->db->select('username');
 $this->db->from('userbe');
 $this->db->where('username', $name);
    $result = $this->db->get();
    return $result->row();
    }*/


public function fetchdata1($id) {

    $id1 = $id;
    $this->db->select('*');
       $this->db->from('userbe');
       $this->db->where('id_admin', $id1);
      $result1 = $this->db->get();
      return $result1->result();

}
    
    public function Delete_admin($checkbox_value)
    {

      for($count = 0; $count < count($checkbox_value); $count++)
              {
                  $id = $checkbox_value[$count];
                  $this->db->where('id_admin', $id);
                  $this->db->delete('userbe'); 

    
   
          
              }

              return '2';
    }

    public function Disable_admin($checkbox_value)
    {
      for($count = 0; $count < count($checkbox_value); $count++)
              {
                  $id = $checkbox_value[$count];
                  $this->db->set('estado', '0', FALSE);
                  $this->db->where('id_admin', $id);
                  $this->db->update('userbe'); 

          
              }

              return '2';
    }
   
 
     public function Enable_admin($checkbox_value)
    {
      for($count = 0; $count < count($checkbox_value); $count++)
              {
                  $id = $checkbox_value[$count];
                  $this->db->set('estado', '1', FALSE);
                  $this->db->where('id_admin', $id);
                  $this->db->update('userbe'); 

          
              }

              return '2';
    }


  }

