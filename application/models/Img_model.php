 <?php
 class Img_model extends CI_Model {
   public function __construct()
   {
     parent::__construct();
   }

   public function upload($param)
   {
     $campos = array(
       'userid'=>$param['userid'],
       'nomImg'=>$param['file_name']
     );
     $this->db->INSERT('img', $campos);
   }
   /*
   *Modelo para mostrar las imagenes en galería
   *
   */
   public function mostrarImg()
   {
      $sql=$this->db->order_by('fecha','DESC')->get('img');

      if($sql->num_rows()>0){
        foreach ($sql->result() as $fila):
          $data[]=$fila;
        endforeach;
      }
      return $data;
    }


  public function addPhoto($param)
  {


    $campos = array(
      'contPost'=>$param['content'],
      'nom_img'=>$param['file_name'],
      'typePost'=>$param['type']
      );
    $this->db->INSERT('publicidad', $campos);
    $idPublicidad = $this->db->insert_id();
    $campos2 = array(
      'idPublicidad_fk'=>$idPublicidad,
      'idAdmin_fk'=>$param['userid']
      );
    $this->db->INSERT('gestiona', $campos2);
  }

  public function get_img($id)
  {
    $this->db->select('gestiona.idPublicidad_fk, gestiona.idAdmin_fk, gestiona.fechaHora, publicidad.nom_img, publicidad.typePost');
    $this->db->from('gestiona');
    $this->db->join('publicidad','publicidad.idPublicidad = gestiona.idPublicidad_fk');
    $this->db->where("publicidad.typePost",2);
    $query = $this->db->order_by('fechaHora','DESC')->get();
    return $query->result();
  }
 }

 ?>
