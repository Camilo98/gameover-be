<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class gest_suscription extends CI_Controller{

  public function __construct()
  {
    parent::__construct();
    $this->load->model('getuser_model');
    $this->load->library('Administrador');
    $this->load->library('User_admin');
    $this->load->library('General_admin');
    $this->load->library('Subscription_admin');
    $this->load->library('Publicity_admin');
    

  }

  function index()
  {
     $estado = 1;
    $pages = $this->getuser_model->getRecordsPagina($estado);
  
    $data = new stdClass();
    $data->title ="Listar" ;
    $data->contenido ="admin/Suscription_view";
    $data->pages= $pages;
    $this->load->view('backend', $data);
  }

  public function updateTipoSuscripcion()
  {   
      $tipo = 0;
      $param = $_POST['suscriptions'];
      foreach($param as $suscripcion)
      {        
        
        $estado = $suscripcion['estado'];
        $idsus = $suscripcion['idsus'];
        $fecha = $suscripcion['fecha'];
    
      //die(print_r($tipo, $id, $fecha));
      $fechaini = strtotime($fecha);
      
      if ($estado == 1) {

       $fechaFin = date('Y-m-d H:i:s', strtotime('+3 month', $fechaini));
       $tipo = 1;
      }elseif ($estado == 2) {

        $fechaFin = date('Y-m-d H:i:s', strtotime('+6 month', $fechaini));
        $tipo = 2;
      }else{

    $fechaFin = date('Y-m-d H:i:s', strtotime('+1 year', $fechaini));
    $tipo = 3;
      }
      
     // $fechaFin = date("Y-m-d");
     //$fechaFin = date("Y-m-d H:i:s", strtotime('+1 year'));


      $param1 = array(
        'idsus'       => $idsus,
        'type'     => $tipo,
        'fecha_fin' => $fechaFin,
        'fecha_ini' => $fecha
      );
        $userbe = new Subscription_admin;
        $opcion = $userbe->updateTipoSuscripcion($param1);
      
}
     echo $opcion;
        //var_dump($opcion);
     
     

}

       public function fetch_suscription(){

        $id = $_POST['id'];
        $userbe = new Subscription_admin;

        $suscriptions = $this->getuser_model->fetch_suscription($id);
         $jsonstring = json_encode($suscriptions);
       echo $jsonstring;
  
      }


public  function listar_suscripciones(){
                        
                  $estado = $_POST['estado'];
                 
                  $userbe = new Subscription_admin;
                  $suscriptions = $userbe->listar_suscripciones($estado);
                    
                
                   
                   $jsonstring = json_encode($suscriptions);
                    //print_r($jsonstring);
                   echo  $jsonstring;
                    


      }




public  function listar_suscripciones_segunestado(){
                        
                  $param = $_POST['param'];
                  $userbe = new Subscription_admin;
                  $estado = $param[0];
                   $estadosus = $param[1];
                   $mostrar = $param[3];

                  if ($estadosus == 1  ) {

                    $suscriptions = $userbe->listar_suscripciones($estado);

                  }else{
                  $suscriptions = $userbe->listar_suscripciones_segunestado($param);
                    }
                //die(print_r($suscriptions));
                   
                   $jsonstring = json_encode($suscriptions);
                    
                   echo  $jsonstring;
                    


      }

      public function updateSuscripcion()
  {   
        $param1 = $_POST['param'];
        $userbe = new Subscription_admin();
        $opcion = $userbe->updateSuscripcion($param1);


        //$this->load->model('getuser_model');
        //$this->getuser_model->Modificar1($param);
        
        //var_dump( $param);
        
    echo $opcion;
        //var_dump($opcion);
     
     

}

public function verificar_vencimiento()
{
if (isset($_POST['mostrar'])){

	$mostrar = $_POST['mostrar'];
}else{
		$mostrar = 0;
}


  if ($mostrar == '1') {
    
  
  $param[0] = 0;
   $param[1] = '0';
   $param[2] = 0;
   $param[3] = $mostrar;
   $suscripciones_notificar = array();
   $fecha_hoy = date("Y-m-d H:i:s");
  $userbe = new Subscription_admin();
  $suscripciones = $userbe->listar_suscripciones_segunestado($param);
 
 
   foreach($suscripciones as $fechas){

      $fechafinsus = $fechas->fecha_fin;
      $nuevafecha = strtotime ( '-1 week' , strtotime ( $fechafinsus ) ) ;
      $fecha = date ( 'Y-m-d H:i:s' , $nuevafecha );// es la fecha fin de suscripcion menos una semana
      //array_push($fechas_finales, $fecha);
      if ($fecha_hoy >= $fecha && $fecha_hoy <= $fechafinsus) {
        array_push($suscripciones_notificar, $fechas);
        
      }
      

  }
  $jsonstring = json_encode($suscripciones_notificar);
  echo $jsonstring;
  //var_dump($param);
}elseif ($mostrar == '0'){
   

   $param[0] = 0;
   $param[1] = '0';
   $param[2] = 0;
$param[3] = $mostrar;
   $suscripciones_notificar = array();
   $fecha_hoy = date("Y-m-d H:i:s");
  $userbe = new Subscription_admin();
  $suscripciones = $userbe->listar_suscripciones_segunestado($param);
  foreach($suscripciones as $fechas){

      $fechafinsus = $fechas->fecha_fin;
      $mail = $fechas->mail_cuenta;

      $nuevafecha = strtotime ( '-1 week' , strtotime ( $fechafinsus ) ) ;
      $fecha = date ( 'Y-m-d H:i:s' , $nuevafecha );// es la fecha fin de suscripcion menos una semana
      //array_push($fechas_finales, $fecha);
      if ($fecha_hoy >= $fecha && $fecha_hoy <= $fechafinsus) {
        array_push($suscripciones_notificar, $mail);
        
      }
  

}

$jsonstring = json_encode($suscripciones_notificar);
  echo $jsonstring;

}
}


public function verificar_suscripcion()
{
if (isset($_POST['mostrar'])){

  $mostrar = $_POST['mostrar'];
}else{
    $mostrar = 0;
}



  $fecha1 = $_POST['fecha'];



  if ($mostrar == '1') {
    
  $susvencida='';
  $param[0] = 0;
   $param[1] = '0';
   $param[2] = 0;
   $param[3] = $mostrar;
   $suscripciones_notificar = array();
   $fecha_hoy = date("Y-m-d H:i:s");
  $userbe = new Subscription_admin();
  $suscripciones = $userbe->listar_suscripciones_segunestado($param);
 
 
   foreach($suscripciones as $fechas){

      $fechafinsus = $fechas->fecha_fin;
      $nuevafecha = strtotime ( '-1 week' , strtotime ( $fechafinsus ) ) ;
      $fecha = date ( 'Y-m-d H:i:s' , $nuevafecha );// es la fecha fin de suscripcion menos una semana
      //array_push($fechas_finales, $fecha);
      if ($fecha_hoy >= $fecha && $fecha_hoy <= $fechafinsus) {
        array_push($suscripciones_notificar, $fechafinsus);
       
        
      }
      

  }
  $susvencida = in_array($fecha1, $suscripciones_notificar);
 
  //$jsonstring = json_encode($suscripciones_notificar);
  //echo $jsonstring;
  //var_dump($param);
  
  if ($susvencida = true) {
    $susvencida = '1';
  }else{
    $susvencida = '0';
  }

}

echo $susvencida;
//var_dump($suscripciones_notificar);
}



public function registrar_notificacion(){

$mail = read_file('/var/www/html/gameover-backend/vencimiento.txt');
$fecha_hoy = date("Y-m-d H:i:s");
 $param[0] = $mail;
   $param[1] = $fecha_hoy;
  $userbe = new Subscription_admin();
  $suscripciones = $userbe->registrar_notificacion($param);


//echo $var;


}


}

?>