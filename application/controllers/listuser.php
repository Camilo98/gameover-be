<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class listuser extends CI_Controller{

  public function __construct()
  {


    parent::__construct();
    $this->load->model('getuser_model');
    $this->load->library('Administrador');
    $this->load->library('User_admin');
    $this->load->library('General_admin');
    $this->load->library('Subscription_admin');
    $this->load->library('Publicity_admin');
    

  }

  function index()
  {
   $estado = 1;
    $users = $this->getuser_model->getRecords($estado);
    $pages = $this->getuser_model->getRecordsPagina($estado);
    $data = new stdClass();
    $data->title ="Listar" ;
    $data->contenido ="admin/listuser_view";
    $data->users= $users;
    $data->pages= $pages;
    $this->load->view('backend', $data);
  }

 public function buscar()
  {
    

      $idAdmin = '';
      $estado_array = '';
      $listuser_array = array();
      $mostrar = $_POST['param'];
      $estado = $mostrar[1];
      $name = $mostrar[0];
      //$listuser_array=[];
      //$mostrar = [$estado, $search];
      
      $userbe = new User_admin();
      $listfriend = $userbe->buscar_admin($name);
     
      
      

      foreach($listfriend as $objeto)
      {        
        
        $idAdmin = $objeto->idAdmin;

        $estado_array = $objeto->estado;

          

        if ($idAdmin != 24) 
        {

          if ($estado == $estado_array) 
          {

          array_push($listuser_array, $objeto);
          
          }

        }

      }
      $jsonstring = json_encode($listuser_array);
       //$jsonstring = json_encode($idAdmin);
       //var_dump($listuser_array);
        
       //$estado1 =$this->mAdmin->Buscar($param, $estado);
       //$mostrar = [$estado, $param];
      echo $jsonstring;
       //var_dump($estado);
       //var_dump($listfriend);
       //}
}


    // Edita datos de usuario pagina
  public function updateTipoSuscripcion()
  {
    
      $tipo=$_GET['tipo'];
      $id=$_GET['idC'];
      $fecha=$_GET['fecha'];
      //die(print_r($fechaini));
      $fechaini = strtotime($fecha);
      if ($tipo == 1) {

       $fechaFin = date('Y-m-d H:i:s', strtotime('+3 month', $fechaini));

      }elseif ($tipo == 2) {
        $fechaFin = date('Y-m-d H:i:s', strtotime('+6 month', $fechaini));
      }else{
    $fechaFin = date('Y-m-d H:i:s', strtotime('+1 year', $fechaini));
      }
      
     // $fechaFin = date("Y-m-d");
     //$fechaFin = date("Y-m-d H:i:s", strtotime('+1 year'));
      $param = array(
        'id'       => $id,
        'type'     => $tipo,
        'fechaFin' => $fechaFin
      );

      $this->getuser_model->updateSuscripcion($param);
      
     redirect(base_url().'listuser');

}

/*

*Borrar los datos del usuario
*
*/

 public function Eliminar(){

        //$u = $_SESSION['userid'];
        
        
          $checkbox_value = $_POST['checkbox_value'];
                  
          $userbe = new User_admin();

          $opcion = $userbe->eliminar_admin($checkbox_value);

         

      echo $opcion;

    }


  public function deshabilitar(){

          //$u = $_SESSION['userid'];
        
        
          $checkbox_value = $_POST['checkbox_value'];

       $userbe = new User_admin();
                  
        $opcion = $userbe->deshabilitar_admin($checkbox_value);
        //$opcion = $checkbox_value;
        
         echo $opcion;
          // return $opcion;
}


    public function habilitar(){

                
          $checkbox_value = $_POST['checkbox_value'];
        
          $userbe = new User_admin();

          $opcion = $userbe->habilitar_admin($checkbox_value);
        
              
              
              echo $opcion;
            
            

      //}
    
    }






  public  function listar_usuario(){
                        
                  $estado = $_POST['estado'];

                    
                   $listuser = $this->getuser_model->getRecords($estado);
                   
                   $jsonstring = json_encode($listuser);
                    //print_r($jsonstring);
                   echo  $jsonstring;
                    


      }


      public  function listar_pagina(){
                        
                   $estado = $_POST['estado'];
                   

                   $listuser = $this->getuser_model->getRecordsPagina($estado);

                
                  $jsonstring = json_encode($listuser);   
                  echo $jsonstring;

      }



       public function fetchuser(){

        $id = $_POST['id'];
        $dataresult = $this->getuser_model->fetchuser($id);
         $jsonstring = json_encode($dataresult);
       echo $jsonstring;
  
      }


      public function fetchpage(){

        $id = $_POST['id'];
        $dataresult = $this->getuser_model->fetchpage($id);
         $jsonstring = json_encode($dataresult);
       echo $jsonstring;
  
      }

       public function Modificar()
      {


        $param = $_POST['param'];
        $userbe = new User_admin();
        $opcion = $userbe->modificar_user($param);


        //$this->load->model('getuser_model');
        //$this->getuser_model->Modificar1($param);
        
        //var_dump( $param);
        echo $opcion;
        
      }


       public function Modificar1()
      {


        $param = $_POST['param'];
        $userbe = new User_admin();
        $opcion = $userbe->modificar_user($param);

        //$this->load->model('getuser_model');
        //$this->getuser_model->Modificar2($param);
        
        //var_dump($param);
        echo $opcion;
      }

      public function GetMuro(){
      
      

        $id1=$_POST['id1'];
        $this->load->model('getuser_model');
        $userbe = new User_admin();
        $tipo1 = $userbe->get_tipouser($id1);
        $tipo = get_object_vars($tipo1);
        $get = $userbe->get_muro($id1, $tipo);
        $nomuser = $userbe->get_user($id1, $tipo);
        
        
   

        $data = new stdClass();
        $data->nomuser = $nomuser;
        $data->tipo1 = $tipo1;
        $data->title ="Muro" ;
        $data->contenido ="muro/muro_user";
        $data->posts = $get;
        $view = $this->load->view('backend',$data, TRUE);
        //$tippo = json_encode($get);
        //var_dump($tippo);
        //$this->set_output($view);

        echo $view;
        //die(print_r($tipo1));
         

        

      }

      public function Del_Publicacion(){

        $id1=$_POST['id'];

        $this->db->set('estado', '0', FALSE);
                  $this->db->where('idPublicacion', $id1);
                  $this->db->update('publicacion'); 

      }

}

?>
