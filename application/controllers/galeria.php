<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class galeria extends CI_Controller{

    public function __construct()
    {
      parent::__construct();
      $this->load->model('Img_model');
/*
$this->load->library('Image_lib');

$this->load->library('upload');*/
    }

    public function index()
    {
      if (empty($_SESSION['is_logged_in']))
      {
        session_destroy();
        redirect();
      }
      else
      {
        $data = new stdClass();
        $data->title ="Galeria" ;
        $data->contenido ="muro/galeria_view";
        $this->load->view('backend',$data);
        $id=$_SESSION['userid'];
        $get = $this->Img_model->get_img($id);
        //echo json_encode($get);
      }
    }


    public function compartir()
    {
     //  die (print_r ($this->post_active()));
     // if($this->post_active()){


          $config['upload_path'] = './assets/uploads/publicidad'; //path folder
          $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp';
          $config['encrypt_name'] = TRUE;
         
          //$this->upload->initialize($config);
         $this->load->library('upload', $config);
     
 
                if(!empty($_FILES['fileimg']['name'])){
                if ($this->upload->do_upload('fileimg')){



                    $gbr = $this->upload->data();
                  //Compress Image
                    $this->_create_thumbs($gbr['file_name']);
                    $this->session->set_flashdata('msg','<div class="alert alert-info">Image Upload Successful.</div>');
                
                  $data2 = array('upload_data' => $this->upload->data());
                  //capturo el nombre del archivo de un array que crea upload
                  $param['file_name'] = $data2['upload_data']['file_name'];
                  $param['pathimg'] = $data2['upload_data']['file_path'];
                  $param['userid'] = $_SESSION['userid'];
                  $param['content']=$this->input->post('compartir');
                  $param['userId']= $_SESSION['userid'];
                  $param['type']= 2;
                  $this->Img_model->addPhoto($param);
                  //echo $param;
                  //$this->load->view('../upimg', $data2);
                  $this->session->set_flashdata("mensaje_success","Subiste correctamente el archivo: " . $param['file_name']);
                  redirect(base_url($_POST['url']));
                    }else{
                    echo $this->upload->display_errors();
                  }
 
            }else{
                    echo "image is empty or type of image not allowed";
            }
     
                
                /*$config['upload_path']          = './assets/uploads/publicidad';
                $config['allowed_types']        = 'gif|jpg|png';
                $config['max_size']             = 2024;
                $config['max_width']            = 1024;
                $config['max_height']           = 768;

                $this->load->library('upload', $config);

                if ( ! $this->upload->do_upload('fileimg'))
                {
                  $error = array('error' => $this->upload->display_errors());
                  $this->session->set_flashdata("mensaje_error","No se pudo subir el archivo");
                  redirect(base_url().'galeria');
                  // $this->load->view('upload_form', $error);
                  //print_r ('error al subir');
                }
                else {
                  $data2 = array('upload_data' => $this->upload->data());
                  //capturo el nombre del archivo de un array que crea upload
                  $param['file_name'] = $data2['upload_data']['file_name'];
                  $param['pathimg'] = $data2['upload_data']['file_path'];
                  $param['userid'] = $_SESSION['userid'];
                  $param['content']=$this->input->post('compartir');
                  $param['userId']= $_SESSION['userid'];
                  $param['type']= 2;
                  $this->Img_model->addPhoto($param);
                  //echo $param;
                  //$this->load->view('../upimg', $data2);
                  $this->session->set_flashdata("mensaje_success","Subiste correctamente el archivo: " . $param['file_name']);
                  redirect(base_url($_POST['url']));
                }*/
           

            }


            function _create_thumbs($filename){
        // Image resizing config

              $config['image_library']    = "gd2";      

        $config['source_image']     = './assets/uploads/publicidad/'.$filename;   

        $config['create_thumb']     = TRUE;      

        $config['maintain_ratio']   = TRUE;      

        $config['width'] = "400";      
          $config['thumb_marker'] = "";
        $config['height'] = "400";
        $config['new_image'] = './assets/uploads/publicidad/thumb/'.$filename;

       /* $config = array(
         
            // Small Image
            array(
                'image_library' => 'GD2',
                'source_image'  => './assets/uploads/publicidad/'.$filename,
                'maintain_ratio'=> FALSE,
                'width'         => 10,
                'height'        => 10,
                'thumb_marker' => '_thumb',
                'new_image'     => './assets/uploads/publicidad/thumb/'.$filename,
                'create_thumb' => TRUE,
            ));*/
 
        $this->load->library('image_lib', $config);
      
           // $this->upload->initialize($item);
            $this->image_lib->resize();
                
            $this->image_lib->clear();
        
    }
      /**
      *Trae las imagenes del usuario
      *
      *
      **/
      public function getImagen(){
        $id=$_SESSION['userid'];
        $this->load->model('Img_model');
        $get = $this->Img_model->get_img($id);
        echo json_encode($get);
      }

      /*
      *
      *metodo para subir imagenes
      *configurar parametros de upload
      *
      */
      /*
      public function subirImagen()
      {
          //print_r('subio');
          $config['upload_path']          = './assets/uploads';
          $config['allowed_types']        = 'gif|jpg|png';
          $config['max_size']             = 2024;
          $config['max_width']            = 1024;
          $config['max_height']           = 768;

          $this->load->library('upload', $config);

          if ( ! $this->upload->do_upload('fileimg'))
          {
            $error = array('error' => $this->upload->display_errors());
            $this->session->set_flashdata("mensaje_error","No se pudo subir el archivo");
            redirect(base_url().'galeria');
            // $this->load->view('upload_form', $error);
            //print_r ('error al subir');
          }
          else {
            $data2 = array('upload_data' => $this->upload->data());
            //capturo el nombre del archivo de un array que crea upload
            $param['file_name'] = $data2['upload_data']['file_name'];
            $param['pathimg'] = $data2['upload_data']['file_path'];
            $param['userid'] = $_SESSION['userid'];
            $param['content']=$this->input->post('compartir');
            $param['userId']= $_SESSION['userid'];
            $param['type']= 2;
            $this->post_model->addPhoto($param);
            //echo $param;
            //$this->load->view('../upimg', $data2);
            $this->session->set_flashdata("mensaje_success","Subiste correctamente el archivo: " . $param['file_name']);
            redirect(base_url($_POST['url']));
          }
      }*/



  }
