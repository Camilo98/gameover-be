<?php
defined('BASEPATH') OR exit('No direct script access allowed');

Class Muro extends CI_Controller {

  public function __construct(){
    parent::__construct();
    $this->load->model('post');
  }


  public function muro(){
    /*creo un objeto $data y le paso los parametros: title al header para hacerlo dinàmico
    en contenido le paso la ruta de mi vista*/
      $data = new stdClass();
      $data->title ="Muro" ;
      $data->contenido ="muro/muro";
      $this->load->view('backend',$data);
  	}

    public function getData()
    {
      $this->load->model('post');
      $get = $this->post->get('title');
      echo json_encode($get);
    }

    public function postData(){

          //Compruebo que el ingreso datos en compartir
          $this->form_validation->set_rules('titulo', 'Titulo', 'required');
          $this->form_validation->set_rules('compartir', 'Compartir', 'required');

          if ($this->form_validation->run() == FALSE)
          {
          /**
          *No hace ninguna accion si no completó los datos obligatorios,
          **/
          $this->muro();
          $this->session->set_flashdata("mensaje_error","Debes colocar Titulo y Contenido");
          //print_r ("llego llego al IF postData");
          }else
                {
                  $param['title']=$this->input->post('titulo');
                  $param['content']=$this->input->post('compartir');
                  $param['userId']= $_SESSION['userid'];


                  $this->post->addpost($param);
                  $this->session->set_flashdata("mensaje_success","Compartido en tu muro");
                  redirect(base_url().'muro');
                  //  print_r ($param);
           }


    }
  }
