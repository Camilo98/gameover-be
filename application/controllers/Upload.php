<?php

class Upload extends CI_Controller {

        public function __construct()
        {
                parent::__construct();
                $this->load->helper(array('form', 'url'));
        }

        public function index()
        {
                $this->load->view('upload_form', array('error' => ' ' ));
        }

       /* public function do_upload()
        {
               /* $config['upload_path']          = './uploads/';
                $config['allowed_types']        = 'gif|jpg|png';
                $config['max_size']             = 1024;
                $config['max_width']            = 1024;
                $config['max_height']           = 768;
*/
                //$this->load->library('upload', $config);

                /*if ( ! $this->upload->do_upload('userfile'))
                {
                        $error = array('error' => $this->upload->display_errors());

                        $this->load->view('upload_form', $error);
                }
                else
                {
                        $data = array('upload_data' => $this->upload->data());

                        $this->load->view('upload_success', $data);
                }
        }
        */

         function _create_thumbs($file_name){
        // Image resizing config
        $config = array(
   
            // Small Image
            array(
                'image_library' => 'GD2',
                'source_image'  => './assets/images/'.$file_name,
                'maintain_ratio'=> FALSE,
                'width'         => 100,
                'height'        => 67,
                'new_image'     => './assets/images/small/'.$file_name
            ));
 
        $this->load->library('upload', $config[0]);
        foreach ($config as $item){
            $this->upload->initialize($item);
            if(!$this->upload->resize()){
                return false;
            }
            $this->upload->clear();
        }
    }
}
?>
