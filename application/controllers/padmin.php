<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class padmin extends CI_Controller{

  public function __construct()
  {
    parent::__construct();
    $this->load->model('mAdmin');
    $this->load->library('Administrador');
    $this->load->library('User_admin');
    $this->load->library('General_admin');
    $this->load->library('Subscription_admin');
    $this->load->library('Publicity_admin');

  }

  public function index()
  {
    $listuser = $this->mAdmin->listar();


    $data = new stdClass();
    $data->title ="Panel Administrador" ;
    $data->contenido ="admin/paneladmin";
    /*$data->listar = $listuser;*/
    $this->load->view('backend',$data);


  }

  public function buscar()
  {
    

      $idAdmin = '';
      $estado_array = '';
      $listuser_array = array();
      $mostrar = $_POST['param'];
      $estado = $mostrar[1];
      $name = $mostrar[0];
      //$listuser_array=[];
      //$mostrar = [$estado, $search];
      
      $userbe = new General_admin();
      $listfriend = $userbe->buscar_admin($name);
     
      
      

      foreach($listfriend as $objeto)
      {        
        
        $idAdmin = $objeto->idAdmin;

        $estado_array = $objeto->estado;

          

        if ($idAdmin != 24) 
        {

          if ($estado == $estado_array) 
          {

          array_push($listuser_array, $objeto);
          
          }

        }

      }
      $jsonstring = json_encode($listuser_array);
       //$jsonstring = json_encode($idAdmin);
       //var_dump($listuser_array);
        
       //$estado1 =$this->mAdmin->Buscar($param, $estado);
       //$mostrar = [$estado, $param];
      echo $jsonstring;
       //var_dump($estado);
       //var_dump($listfriend);
       //}
}



function AddAdmin(){



  $data = new stdClass();
  $data->title ="Registro";
  $data->contenido ="admin/registro";
  $this->load->view('backend',$data);



}

function addUser(){

  $this->load->model('mAdmin');

        //Compruebo que un email válido, contraseña, nombre, etc. han sido ingresados

  $this->form_validation->set_rules('username','Nombre', 'required');
  $this->form_validation->set_rules('apellido', 'Apellido', 'required');
  $this->form_validation->set_rules('correo', 'Email', 'trim|required|valid_email');
  $this->form_validation->set_rules('pass', 'Password', 'required');
  $this->form_validation->set_rules('repass', 'Password', 'required|matches[pass]');
  $this->form_validation->set_rules('nombre', 'Nombre', 'required');
  $this->form_validation->set_rules('rol', 'Rol', 'required');

  if ($this->form_validation->run() == FALSE)
        {   //chequea que el correo es un correo válido
          $is_form_ok = FALSE;

          if (form_error('correo'))
          {
            $is_form_ok = FALSE;
            $this->AddAdmin();
            $this->session->set_flashdata("mensaje_error","Ingrese un correo válido");
            //echo "errorcorreo";
            //chequea que las contraseñas sean iguales
          }else if(form_error('repass'))
          {
            $this->AddAdmin();
            $this->session->set_flashdata("mensaje_error","Las contraseñas no coinciden");
            //echo "errorrepass";
          }else 
              {
                /*
                te devuele a registro(); si no completó los datos obligatorios
                */
                $is_form_ok= FALSE;
                $this->AddAdmin();
                $this->session->set_flashdata("mensaje_error","Todos los campos son requeridos");
                //echo "errorultimo";
                //print_r ("llego llego al IF adduser");
              }

            }else
             {

              
              //$resultado = $this->mAdmin->verificar($username);
              //die(print_r($resultado));
            $rol = $this->input->post('rol');

            if ($rol == 'General Admin') {

              $userbe = new General_admin();

            }elseif ($rol == 'User Admin') {
              
              $userbe = new User_admin();

            }elseif ($rol == 'Subscription Admin') {
              
              $userbe = new Subscription_admin();

            }else{

              $userbe = new Publicity_admin();
            }
          
          $userbe->set_correo($this->input->post('correo'));
          $userbe->set_nombre($this->input->post('nombre'));
          $userbe->set_apellido($this->input->post('apellido'));
          $userbe->set_pass($this->input->post('pass'));
          $userbe->set_username($this->input->post('username'));
          $userbe->set_rol($this->input->post('rol'));
          //++-----------------------------------------------
          $usuario_creado = $userbe->verif_user($userbe);

          //--ERROR_002 Comprueba correo no esté en uso--//
          if($usuario_creado=="ERROR_002")
          {
          //die(print_r('existo', true));
            $this->session->set_flashdata("mensaje_error","El correo ya está en uso");
            $this->index();
          }
          else
            {
              $this->session->set_flashdata("mensaje_success","Registro Correcto Inicia sesión con el usuario: " . $usuario_creado);
              session_destroy();
              redirect(base_url('login'));
              //die (print_r($usuario->get_email(),TRUE));
            }
           
               
          }


        }

          public function Eliminar(){

           $u = $_SESSION['userid'];
            
           $checkbox_value = $_POST['checkbox_value'];

         
              $userbe = new General_admin();
              $opcion= $userbe->eliminar_admin($checkbox_value);
              
              echo $opcion; 
                                                        
        }


      public function Deshabilitar(){

                
          $checkbox_value = $_POST['checkbox_value'];
        
          $userbe = new General_admin();
          $opcion = $userbe->deshabilitar_admin($checkbox_value);

              echo $opcion;
            
            
    
    }

     public function Habilitar(){

                
          $checkbox_value = $_POST['checkbox_value'];
        
          $userbe = new General_admin();
          $opcion = $userbe->habilitar_admin($checkbox_value);

              echo $opcion;
        
              
    
    }

     
      public function Modificar()
      {


        $param = $_POST['param'];

        $userbe = new General_admin();
        $userbe->modificar_admin($param);
        
        
        
        //var_dump( $param);
        
      }


      public  function listar(){

                  $estado = $_POST['estado'];
                  
                     
                   $listuser = $this->mAdmin->listar($estado);


                   $jsonstring = json_encode($listuser);
                  // $prueba = [$u, $jsonstring];
                   echo $jsonstring;

      }
    

      public function fetchdata(){

        $id = $_POST['id'];
        $dataresult = $this->mAdmin->fetchdata1($id);
         $jsonstring = json_encode($dataresult);
       echo $jsonstring;

      }

  

}
