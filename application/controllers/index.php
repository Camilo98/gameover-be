<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Index extends CI_Controller {
  public function __construct(){
    parent::__construct();
  }
	public function index(){
    /*creo un objeto $data y le paso los parametros: title al header para hacerlo dinàmico
    en contenido le paso la ruta de mi vista*/
    $data = new stdClass();
    $data->title ="Red GO!";
    $data->contenido ="auth/login";
    $this->load->view('backend',$data);
	}
}
