<?php
defined('BASEPATH') OR exit('No direct script access allowed');

Class auth extends CI_Controller {

  public function __construct(){
    parent::__construct();
    $this->load->model('Auth_model');
    $this->load->library('General_admin');
  }


  public function login(){
    

    /*creo un objeto $data y le paso los parametros: title al header para hacerlo dinàmico
    en contenido le paso la ruta de mi vista*/
      $data = new stdClass();
      $data->title ="Login" ;
      $data->contenido ="auth/login";
      $this->load->view('backend',$data);
  	}

    public function signin()
    {
      //Compruebo que el email y la contraseña han sido ingresados
      $this->form_validation->set_rules('username', 'Usuario', 'required');
      $this->form_validation->set_rules('pass', 'Password', 'required');
      if ($this->form_validation->run() == FALSE)
      {
        $this->login();
      }
      else
      {
        $username = $this->input->post('username');
        $pass=$this->input->post('pass');

        $userbe=Administrador::verif_login($username, $pass);

        if(!$userbe){
          $this->session->set_flashdata('mensaje_error', 'Datos incorrectos');
          
          redirect(base_url().'login');
          
        }elseif ($userbe == "ERROR_001") {
          $this->session->set_flashdata('mensaje_error', 'Su usuario esta deshabilitado');
          
          redirect(base_url().'login');
        }else{
       
        //si paso todo lo anterior se crea la session
        $_SESSION['userid'] = $userbe->get_id_admin();
        $_SESSION['email'] = $userbe->get_correo();
        $_SESSION['nomuser'] = $userbe->get_nombre();
        $_SESSION['apeuser'] = $userbe->get_apellido();//$user->nomUser;
        $_SESSION['rol'] = $userbe->get_rol();
        $_SESSION['is_logged_in'] = TRUE;
        $this->session->set_flashdata("mensaje_success","Bienvenido: " . $_SESSION['nomuser']);
        $this->session->set_tempdata("mensaje_success",  5);

       $rol = $_SESSION['rol']; 

       print_r($rol);
        if($rol == 'General Admin'){


          redirect(base_url().'padmin');



        }elseif ($rol == 'Publicity Admin') {

                    redirect(base_url().'galeria');

          
        }elseif ($rol == 'User Admin') {

                    redirect(base_url().'listar');

          
        }elseif ($rol == 'Subscription Admin') {

                    redirect(base_url().'gest_suscription');

          
        }

        }


        //redirect(base_url().'padmin');
      }



        

       
    }

    public function logout()
    {
      session_destroy();
      redirect();
    }

}
