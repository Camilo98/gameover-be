-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 28-06-2020 a las 15:48:50
-- Versión del servidor: 10.4.8-MariaDB
-- Versión de PHP: 7.1.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `gameover`
--

--
-- Crea base de datos gameover
--
CREATE database if NOT EXISTS gameover;
--
-- Usa Base de dato gameover
--
USE gameover;
-- Base de datos: `gameover`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `administra`
--

CREATE TABLE `administra` (
  `id_admin_fk` int(10) NOT NULL,
  `id_cuenta_fk` int(10) NOT NULL,
  `fecha_ini` timestamp NOT NULL DEFAULT current_timestamp(),
  `fecha_fin` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `fecha_hora` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `accion` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `autoriza`
--

CREATE TABLE `autoriza` (
  `id_admin_fk` int(10) NOT NULL,
  `id_suscrip_fk` int(10) NOT NULL,
  `fecha_hora` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comenta`
--

CREATE TABLE `comenta` (
  `id_comenta` int(10) NOT NULL,
  `id_cuenta_fk` int(10) NOT NULL,
  `id_publicacion_fk` int(10) NOT NULL,
  `texto` varchar(200) NOT NULL,
  `fecha_hora` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `comenta`
--

INSERT INTO `comenta` (`id_comenta`, `id_cuenta_fk`, `id_publicacion_fk`, `texto`, `fecha_hora`) VALUES
(1, 24, 132, 'pero que porqueria de planta', '2019-11-19 02:09:26'),
(2, 24, 132, 'Siii muy fea', '2019-11-19 02:16:16'),
(3, 24, 132, 'puff', '2019-11-20 01:04:30'),
(4, 24, 98, 'l', '2019-11-22 01:08:29'),
(5, 24, 137, 'lost', '2019-11-22 01:08:50'),
(6, 24, 110, 'hola sera', '2019-11-22 04:12:34'),
(7, 24, 137, 'ff', '2019-11-23 03:51:03'),
(8, 24, 138, 'jjjjj', '2019-11-24 01:06:25'),
(9, 24, 139, 'kkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk', '2019-11-25 17:58:29'),
(10, 24, 139, 'hola', '2019-11-26 00:06:34'),
(11, 24, 140, 'Me gusta negrita y torcida', '2019-11-27 20:59:52'),
(12, 9, 175, 'Nuevo comentario en grupo', '2020-06-09 03:21:46'),
(13, 24, 32, 'Bastante triste lo tuyo', '2020-06-17 23:08:12'),
(14, 24, 300, 'comentado', '2020-06-21 00:18:28'),
(15, 24, 300, 'nuevamente comento', '2020-06-21 00:19:38');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comparte`
--

CREATE TABLE `comparte` (
  `id_cuenta_fk` int(10) NOT NULL,
  `id_publicacion_fk` int(10) NOT NULL,
  `date_post` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `comparte`
--

INSERT INTO `comparte` (`id_cuenta_fk`, `id_publicacion_fk`, `date_post`) VALUES
(6, 32, '2018-07-08 21:29:40'),
(9, 10, '2018-06-16 00:24:59'),
(9, 56, '2018-07-10 21:17:00'),
(9, 57, '2018-07-10 22:18:21'),
(9, 58, '2018-07-10 22:43:31'),
(9, 59, '2018-07-10 22:44:16'),
(9, 60, '2018-07-10 22:55:32'),
(9, 61, '2018-07-10 23:02:40'),
(9, 62, '2018-07-10 23:14:12'),
(9, 63, '2018-07-11 00:40:35'),
(9, 64, '2018-07-11 00:49:24'),
(9, 65, '2018-07-11 01:11:21'),
(9, 66, '2018-07-11 01:12:42'),
(9, 67, '2018-07-11 01:14:51'),
(9, 68, '2018-07-11 01:18:30'),
(9, 69, '2018-07-11 01:19:13'),
(9, 70, '2018-07-11 01:20:18'),
(9, 71, '2018-07-11 01:20:33'),
(9, 72, '2018-07-11 01:22:46'),
(9, 73, '2018-07-11 02:00:50'),
(9, 74, '2018-07-11 02:06:11'),
(9, 75, '2018-07-11 03:34:22'),
(9, 98, '2018-07-12 19:56:42'),
(9, 99, '2018-07-12 20:32:30'),
(12, 15, '2018-07-01 23:01:54'),
(12, 16, '2018-07-01 23:02:01'),
(13, 14, '2018-06-29 00:49:10'),
(15, 31, '2018-07-08 18:14:30'),
(15, 33, '2018-07-08 21:36:27'),
(15, 34, '2018-07-08 22:36:59'),
(15, 35, '2018-07-08 23:46:30'),
(15, 101, '2018-07-13 01:25:34'),
(15, 102, '2018-07-13 01:35:27'),
(15, 103, '2018-07-13 02:35:28'),
(15, 104, '2018-07-13 02:39:01'),
(15, 105, '2018-07-13 02:50:52'),
(15, 106, '2018-07-13 02:56:05'),
(17, 18, '2018-07-05 16:18:55'),
(18, 76, '2018-07-11 03:37:52'),
(18, 77, '2018-07-11 03:41:19'),
(18, 78, '2018-07-11 05:01:05'),
(18, 79, '2018-07-11 05:01:23'),
(23, 26, '2018-07-08 00:48:04'),
(23, 27, '2018-07-08 00:48:13'),
(23, 28, '2018-07-08 00:51:06'),
(23, 29, '2018-07-08 02:47:12'),
(23, 30, '2018-07-08 02:48:20'),
(23, 100, '2018-07-12 21:59:56'),
(24, 21, '2018-07-07 22:18:58'),
(24, 22, '2018-07-07 22:27:07'),
(24, 23, '2018-07-07 22:31:15'),
(24, 24, '2018-07-07 22:35:23'),
(24, 25, '2018-07-07 22:35:43'),
(24, 36, '2018-07-09 15:41:34'),
(24, 37, '2018-07-09 16:08:09'),
(24, 38, '2018-07-09 20:01:26'),
(24, 39, '2018-07-09 21:04:59'),
(24, 40, '2018-07-09 21:13:15'),
(24, 41, '2018-07-09 21:15:51'),
(24, 42, '2018-07-09 21:17:35'),
(24, 43, '2018-07-09 21:31:17'),
(24, 44, '2018-07-09 21:35:57'),
(24, 45, '2018-07-09 21:36:59'),
(24, 46, '2018-07-09 21:37:27'),
(24, 47, '2018-07-09 21:49:31'),
(24, 48, '2018-07-09 22:31:48'),
(24, 49, '2018-07-09 23:53:47'),
(24, 50, '2018-07-09 23:54:24'),
(24, 51, '2018-07-09 23:55:08'),
(24, 52, '2018-07-10 15:10:56'),
(24, 53, '2018-07-10 19:33:30'),
(24, 54, '2018-07-10 19:35:21'),
(24, 55, '2018-07-10 19:36:32'),
(24, 80, '2018-07-11 16:47:01'),
(24, 81, '2018-07-11 17:02:44'),
(24, 82, '2018-07-11 17:22:50'),
(24, 83, '2018-07-11 17:25:01'),
(24, 84, '2018-07-11 17:28:51'),
(24, 85, '2018-07-11 17:33:13'),
(24, 86, '2018-07-11 17:33:58'),
(24, 87, '2018-07-11 17:35:34'),
(24, 88, '2018-07-11 17:48:06'),
(24, 89, '2018-07-11 17:50:11'),
(24, 90, '2018-07-11 18:08:05'),
(24, 91, '2018-07-11 20:08:14'),
(24, 92, '2018-07-11 20:09:29'),
(24, 93, '2018-07-12 00:52:35'),
(24, 94, '2018-07-12 00:53:21'),
(24, 95, '2018-07-12 00:53:38'),
(24, 96, '2018-07-12 00:53:57'),
(24, 97, '2018-07-12 00:54:47'),
(24, 129, '2018-07-19 01:11:27'),
(24, 130, '2018-07-19 01:17:00'),
(24, 131, '2018-07-19 04:35:45'),
(24, 132, '2018-08-05 17:51:18'),
(24, 134, '2019-11-19 02:47:40'),
(24, 135, '2019-11-19 02:58:22'),
(24, 136, '2019-11-21 00:31:51'),
(24, 137, '2019-11-22 00:29:38'),
(24, 138, '2019-11-24 00:59:36'),
(24, 139, '2019-11-24 02:01:58'),
(24, 140, '2019-11-26 04:25:10'),
(24, 168, '2020-05-21 04:04:51'),
(24, 169, '2020-05-22 23:05:46'),
(24, 170, '2020-05-23 01:24:27'),
(24, 171, '2020-06-04 01:04:33'),
(24, 172, '2020-06-04 01:07:28'),
(24, 173, '2020-06-04 14:04:33'),
(24, 174, '2020-06-08 00:27:45'),
(24, 177, '2020-06-11 00:10:14'),
(24, 178, '2020-06-11 00:10:22'),
(24, 181, '2020-06-11 04:28:00'),
(24, 182, '2020-06-11 04:40:37'),
(24, 183, '2020-06-11 04:41:10'),
(24, 184, '2020-06-11 23:45:17'),
(24, 186, '2020-06-11 23:50:11'),
(24, 188, '2020-06-11 23:56:07'),
(24, 189, '2020-06-11 23:56:30'),
(24, 190, '2020-06-12 00:52:26'),
(24, 191, '2020-06-12 00:57:29'),
(24, 192, '2020-06-12 02:09:53'),
(24, 193, '2020-06-12 02:12:45'),
(24, 194, '2020-06-16 04:18:32'),
(24, 195, '2020-06-16 04:24:13'),
(24, 196, '2020-06-16 19:04:59'),
(24, 197, '2020-06-16 19:06:35'),
(24, 198, '2020-06-16 19:06:46'),
(24, 199, '2020-06-16 21:51:52'),
(24, 200, '2020-06-16 21:52:16'),
(24, 208, '2020-06-16 23:31:34'),
(24, 209, '2020-06-16 23:34:47'),
(24, 210, '2020-06-16 23:35:55'),
(24, 220, '2020-06-17 01:38:34'),
(24, 221, '2020-06-17 01:56:19'),
(24, 222, '2020-06-17 01:57:12'),
(24, 224, '2020-06-17 02:07:19'),
(24, 225, '2020-06-17 02:07:19'),
(24, 226, '2020-06-17 02:08:25'),
(24, 233, '2020-06-17 03:47:56'),
(24, 238, '2020-06-17 04:30:04'),
(24, 239, '2020-06-17 04:30:28'),
(24, 244, '2020-06-17 19:33:57'),
(24, 246, '2020-06-17 19:44:46'),
(24, 247, '2020-06-17 23:09:48'),
(24, 249, '2020-06-17 23:30:01'),
(24, 250, '2020-06-17 23:31:53'),
(24, 252, '2020-06-18 00:04:03'),
(24, 253, '2020-06-18 00:05:46'),
(24, 254, '2020-06-18 00:06:14'),
(24, 255, '2020-06-18 00:16:02'),
(24, 256, '2020-06-18 00:16:43'),
(24, 257, '2020-06-18 00:17:08'),
(24, 258, '2020-06-18 00:17:13'),
(24, 259, '2020-06-18 00:17:45'),
(24, 260, '2020-06-18 00:18:01'),
(24, 261, '2020-06-18 00:24:23'),
(24, 262, '2020-06-18 00:28:24'),
(24, 263, '2020-06-18 00:32:15'),
(24, 264, '2020-06-18 00:32:56'),
(24, 265, '2020-06-18 00:35:53'),
(24, 266, '2020-06-18 00:38:11'),
(24, 267, '2020-06-18 00:39:16'),
(24, 268, '2020-06-18 00:40:00'),
(24, 269, '2020-06-18 00:40:07'),
(24, 270, '2020-06-18 00:40:57'),
(24, 271, '2020-06-18 00:42:34'),
(24, 272, '2020-06-18 00:44:06'),
(24, 273, '2020-06-18 01:10:32'),
(24, 274, '2020-06-18 01:12:55'),
(24, 275, '2020-06-18 01:16:05'),
(24, 276, '2020-06-18 01:16:24'),
(24, 277, '2020-06-18 01:22:09'),
(24, 278, '2020-06-18 01:22:56'),
(24, 279, '2020-06-18 01:23:30'),
(24, 280, '2020-06-18 01:25:00'),
(24, 281, '2020-06-18 01:26:10'),
(24, 282, '2020-06-18 01:29:11'),
(24, 283, '2020-06-18 01:29:26'),
(24, 284, '2020-06-18 01:29:52'),
(24, 285, '2020-06-18 01:53:05'),
(24, 286, '2020-06-18 01:53:53'),
(24, 287, '2020-06-18 01:54:17'),
(24, 288, '2020-06-18 01:57:06'),
(24, 289, '2020-06-18 02:46:37'),
(24, 290, '2020-06-18 05:45:09'),
(24, 291, '2020-06-18 19:02:11'),
(24, 293, '2020-06-19 05:40:31'),
(24, 294, '2020-06-20 00:57:44'),
(24, 295, '2020-06-20 01:25:42'),
(24, 296, '2020-06-20 02:31:48'),
(24, 297, '2020-06-20 02:32:18'),
(24, 299, '2020-06-20 23:33:08'),
(24, 300, '2020-06-20 23:59:51'),
(24, 304, '2020-06-28 04:03:45'),
(28, 107, '2018-07-13 04:41:19'),
(28, 108, '2018-07-13 04:42:30'),
(28, 109, '2018-07-13 05:00:58'),
(28, 110, '2018-07-13 05:12:01'),
(28, 111, '2018-07-13 05:18:25'),
(38, 124, '2018-07-14 00:04:42'),
(39, 123, '2018-07-13 22:08:59'),
(43, 133, '2019-08-19 15:50:47'),
(61, 303, '2020-06-26 03:27:51');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `controla`
--

CREATE TABLE `controla` (
  `id_admin_fk` int(10) NOT NULL,
  `id_publicacion_fk` int(10) NOT NULL,
  `fecha_hora` timestamp NOT NULL DEFAULT current_timestamp(),
  `accion` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cuenta`
--

CREATE TABLE `cuenta` (
  `id_cuenta` int(10) NOT NULL,
  `pass_cuenta` varchar(100) NOT NULL,
  `cel_cuenta` int(11) NOT NULL,
  `mail_cuenta` varchar(100) NOT NULL,
  `foto_perfil` varchar(45) DEFAULT NULL,
  `estado` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `cuenta`
--

INSERT INTO `cuenta` (`id_cuenta`, `pass_cuenta`, `cel_cuenta`, `mail_cuenta`, `foto_perfil`, `estado`) VALUES
(6, '63982e54a7aeb0d89910475ba6dbd3ca6dd4e5a1', 2147483647, 'bloqueado@mail.com', 'user-default.png', 0),
(7, '63982e54a7aeb0d89910475ba6dbd3ca6dd4e5a1', 9876, 'egarzon@mail.com', 'user-default.png', 1),
(9, '63982e54a7aeb0d89910475ba6dbd3ca6dd4e5a1', 3335444, 'burro@mail.com', 'user-default.png', 1),
(11, '63982e54a7aeb0d89910475ba6dbd3ca6dd4e5a1', 555666, 'cosentino@camilo.com', 'user-default.png', 1),
(12, '63982e54a7aeb0d89910475ba6dbd3ca6dd4e5a1', 123432, 'elver@gudo.com', 'user-default.png', 1),
(13, '63982e54a7aeb0d89910475ba6dbd3ca6dd4e5a1', 1, 'ss@ss.com', 'user-default.png', 1),
(14, '63982e54a7aeb0d89910475ba6dbd3ca6dd4e5a1', 5566, 'rolon@mail.com', 'user-default.png', 1),
(15, '63982e54a7aeb0d89910475ba6dbd3ca6dd4e5a1', 1234, 'pegajoso@mail.com', 'user-default.png', 1),
(16, '63982e54a7aeb0d89910475ba6dbd3ca6dd4e5a1', 1234, 'prosa@mail.com', 'user-default.png', 1),
(17, '63982e54a7aeb0d89910475ba6dbd3ca6dd4e5a1', 1234, 'elodid@mail.com', 'user-default.png', 1),
(18, '63982e54a7aeb0d89910475ba6dbd3ca6dd4e5a1', 1234, 'pchopito@algo.com', 'user-default.png', 1),
(19, '63982e54a7aeb0d89910475ba6dbd3ca6dd4e5a1', 1234, 'pepapig@mail.com', 'user-default.png', 1),
(20, '63982e54a7aeb0d89910475ba6dbd3ca6dd4e5a1', 1234, 'pguerra@mail.com', 'user-default.png', 1),
(21, '63982e54a7aeb0d89910475ba6dbd3ca6dd4e5a1', 222, 'dafda@mama.com', 'user-default.png', 1),
(22, '63982e54a7aeb0d89910475ba6dbd3ca6dd4e5a1', 1234, 'fafo@mail.com', 'user-default.png', 1),
(23, '63982e54a7aeb0d89910475ba6dbd3ca6dd4e5a1', 1234, 'pedro@mail.com', 'user-default.png', 1),
(24, '63982e54a7aeb0d89910475ba6dbd3ca6dd4e5a1', 7780000, 'fabian@mail.com', '2a01968e1aa7ca0326398d65bbda22df.jpg', 1),
(25, '63982e54a7aeb0d89910475ba6dbd3ca6dd4e5a1', 1234, 'jtalarga@mail.com', 'user-default.png', 1),
(26, '63982e54a7aeb0d89910475ba6dbd3ca6dd4e5a1', 12121212, 'caramelos@chupetines.com', 'user-default.png', 1),
(27, '63982e54a7aeb0d89910475ba6dbd3ca6dd4e5a1', 123456, 'capitan@america.com', 'user-default.png', 1),
(28, '63982e54a7aeb0d89910475ba6dbd3ca6dd4e5a1', 1234, 'makelele@mail.com', 'user-default.png', 1),
(29, '63982e54a7aeb0d89910475ba6dbd3ca6dd4e5a1', 1234, '1234@mail.com', 'user-default.png', 1),
(31, '63982e54a7aeb0d89910475ba6dbd3ca6dd4e5a1', 1234, '123@mail.com', 'user-default.png', 1),
(33, '63982e54a7aeb0d89910475ba6dbd3ca6dd4e5a1', 1234, 'milesdemal@mail.com', 'user-default.png', 1),
(37, '63982e54a7aeb0d89910475ba6dbd3ca6dd4e5a1', 1234, 'cuentapagina@mail.com', 'user-default.png', 1),
(38, '63982e54a7aeb0d89910475ba6dbd3ca6dd4e5a1', 1234, 'pagina@mail.com', 'user-default.png', 1),
(39, '63982e54a7aeb0d89910475ba6dbd3ca6dd4e5a1', 4444, 'romuso@mail.com', 'user-default.png', 1),
(41, '63982e54a7aeb0d89910475ba6dbd3ca6dd4e5a1', 1234, 'pagina2@mail.com', 'user-default.png', 1),
(42, '63982e54a7aeb0d89910475ba6dbd3ca6dd4e5a1', 1234, 'pagina3@mail.com', 'user-default.png', 1),
(43, '63982e54a7aeb0d89910475ba6dbd3ca6dd4e5a1', 1234, 'fabian@gmail.com', 'user-default.png', 1),
(44, '63982e54a7aeb0d89910475ba6dbd3ca6dd4e5a1', 900877378, 'aparato@mail.com', 'user-default.png', 1),
(45, '63982e54a7aeb0d89910475ba6dbd3ca6dd4e5a1', 444444, 'paginanueva@mail.com', NULL, 0),
(46, '63982e54a7aeb0d89910475ba6dbd3ca6dd4e5a1', 655684, 'ragala@mail.com', 'user-default.png', 1),
(47, '63982e54a7aeb0d89910475ba6dbd3ca6dd4e5a1', 5564845, 'aparato25@mail.com', 'user-default.png', 1),
(48, '63982e54a7aeb0d89910475ba6dbd3ca6dd4e5a1', 44444, 'fabian5@mail.com', 'user-default.png', 1),
(49, '63982e54a7aeb0d89910475ba6dbd3ca6dd4e5a1', 44444, 'fabian6@mail.com', 'user-default.png', 1),
(50, '63982e54a7aeb0d89910475ba6dbd3ca6dd4e5a1', 22222, 'fabian10@mail.com', 'user-default.png', 1),
(51, '63982e54a7aeb0d89910475ba6dbd3ca6dd4e5a1', 1234, 'fabian14@mail.com', 'user-default.png', 1),
(52, '63982e54a7aeb0d89910475ba6dbd3ca6dd4e5a1', 1234, 'fabian16@mail.com', 'user-default.png', 1),
(53, '63982e54a7aeb0d89910475ba6dbd3ca6dd4e5a1', 1234, 'fasd@mail.com', 'user-default.png', 1),
(54, '63982e54a7aeb0d89910475ba6dbd3ca6dd4e5a1', 1234, 'fabi044@mail.com', 'user-default.png', 1),
(55, '63982e54a7aeb0d89910475ba6dbd3ca6dd4e5a1', 1234, 'fabi0445@mail.com', 'user-default.png', 1),
(56, '63982e54a7aeb0d89910475ba6dbd3ca6dd4e5a1', 1234, 'fabi0099@mail.com', 'user-default.png', 1),
(57, '63982e54a7aeb0d89910475ba6dbd3ca6dd4e5a1', 12344, 'caperalta@mail.com', 'user-default', 1),
(58, '63982e54a7aeb0d89910475ba6dbd3ca6dd4e5a1', 2147483647, 'pagin_nueva@pagina.com', 'user-default', 1),
(59, '63982e54a7aeb0d89910475ba6dbd3ca6dd4e5a1', 1234, 'chochico@mail.com', 'user-default', 1),
(60, '63982e54a7aeb0d89910475ba6dbd3ca6dd4e5a1', 1234, 'apireta@mail.com', 'user-default', 1),
(61, '63982e54a7aeb0d89910475ba6dbd3ca6dd4e5a1', 1234, 'verde@mail.com', 'user-default', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `es_amigo`
--

CREATE TABLE `es_amigo` (
  `id_cuenta_fk_u1` int(10) NOT NULL,
  `id_cuenta_fk_u2` int(10) NOT NULL,
  `estado` tinyint(1) DEFAULT NULL,
  `inicio_amistad` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `es_amigo`
--

INSERT INTO `es_amigo` (`id_cuenta_fk_u1`, `id_cuenta_fk_u2`, `estado`, `inicio_amistad`) VALUES
(9, 22, 1, NULL),
(9, 26, 1, NULL),
(9, 27, 1, NULL),
(12, 11, 0, NULL),
(15, 12, 0, NULL),
(15, 13, 0, NULL),
(18, 9, 0, NULL),
(23, 22, 0, NULL),
(24, 6, 1, '2019-11-24 02:04:12'),
(24, 9, 1, '2020-06-10 03:02:04'),
(24, 13, 1, '2019-11-30 16:25:40'),
(24, 22, 1, '2020-06-20 18:12:21'),
(24, 51, 1, '2019-11-25 17:37:36'),
(24, 53, 1, '2019-11-24 02:57:48'),
(25, 6, 0, NULL),
(25, 21, 0, NULL),
(28, 24, 1, NULL),
(39, 17, 1, NULL),
(57, 22, 1, '2019-11-26 21:33:45'),
(57, 24, 1, '2019-11-26 21:34:05'),
(57, 50, 1, '2019-11-26 21:33:55'),
(61, 61, 0, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `es_miembro`
--

CREATE TABLE `es_miembro` (
  `id_cuenta_fk` int(10) NOT NULL,
  `id_grupo_fk` int(10) NOT NULL,
  `rol` tinyint(1) NOT NULL DEFAULT 3 COMMENT '-2 Bloqueado, -1 Baja, 1 Dios,2 Admin, 3 Mortal',
  `fecha_ini` timestamp NOT NULL DEFAULT current_timestamp(),
  `fecha_fin` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `es_miembro`
--

INSERT INTO `es_miembro` (`id_cuenta_fk`, `id_grupo_fk`, `rol`, `fecha_ini`, `fecha_fin`) VALUES
(9, 1, 3, '2020-05-08 17:58:06', NULL),
(24, 1, -1, '2020-05-08 17:58:06', '2020-06-24 00:46:06'),
(61, 1, -1, '2020-06-26 03:28:03', '2020-06-26 05:45:54'),
(9, 2, 3, '2020-06-21 04:10:33', NULL),
(24, 2, 3, '2020-05-08 17:58:06', '2020-06-21 20:45:55'),
(61, 2, 3, '2020-06-26 03:46:13', NULL),
(9, 3, 3, '2020-06-21 04:50:05', NULL),
(24, 3, -1, '2020-06-23 21:14:37', '2020-06-24 00:32:17'),
(27, 4, 1, '2020-05-08 17:58:06', NULL),
(24, 8, 1, '2020-05-08 22:55:05', NULL),
(9, 9, 3, '2020-06-21 04:39:29', NULL),
(24, 9, 1, '2020-05-08 22:56:17', NULL),
(9, 10, 3, '2020-06-21 04:40:55', NULL),
(24, 10, 1, '2020-05-08 22:56:58', '2020-06-17 08:00:00'),
(24, 11, 1, '2020-05-08 23:05:05', NULL),
(24, 12, 1, '2020-05-08 23:05:37', NULL),
(24, 13, 1, '2020-05-08 23:56:39', NULL),
(24, 14, 1, '2020-05-09 00:01:53', NULL),
(24, 15, 1, '2020-05-09 00:02:17', NULL),
(24, 16, 1, '2020-05-09 02:12:37', NULL),
(24, 17, 1, '2020-05-09 02:56:15', NULL),
(24, 18, 1, '2020-05-09 02:56:30', NULL),
(24, 19, 1, '2020-05-09 16:59:27', NULL),
(24, 20, 1, '2020-05-09 17:12:51', NULL),
(24, 21, 1, '2020-05-09 17:20:15', NULL),
(24, 22, 1, '2020-05-09 17:52:36', NULL),
(24, 23, 1, '2020-05-09 19:32:04', NULL),
(24, 24, 1, '2020-05-10 00:51:17', NULL),
(9, 25, 3, '2020-06-21 04:14:07', NULL),
(9, 26, 1, '2020-05-10 02:36:04', NULL),
(9, 27, 1, '2020-05-10 02:38:47', NULL),
(9, 28, 1, '2020-05-10 02:41:27', NULL),
(9, 29, -1, '2020-05-10 02:59:08', '2020-06-21 08:17:58'),
(24, 30, 1, '2020-05-17 21:18:32', NULL),
(24, 31, 1, '2020-06-21 00:01:51', NULL),
(24, 32, 1, '2020-06-21 00:34:26', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `gestiona`
--

CREATE TABLE `gestiona` (
  `id_admin_fk` int(10) NOT NULL,
  `id_publicidad_fk` int(10) NOT NULL,
  `fecha_hora` timestamp NOT NULL DEFAULT current_timestamp(),
  `accion` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `grupo`
--

CREATE TABLE `grupo` (
  `id_grupo` int(10) NOT NULL,
  `nombre_grupo` varchar(100) NOT NULL,
  `foto_grupo` varchar(200) NOT NULL DEFAULT 'default_grupo.png',
  `desc_grupo` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `grupo`
--

INSERT INTO `grupo` (`id_grupo`, `nombre_grupo`, `foto_grupo`, `desc_grupo`) VALUES
(1, 'Grupo 1', 'default_grupo.png', ''),
(2, 'Grupo 2', 'default_grupo.png', ''),
(3, 'Grupo 3', 'default_grupo.png', ''),
(4, 'Grupo 4', 'default_grupo.png', ''),
(5, 'Grupo 5', 'default_grupo.png', ''),
(8, 'fasd', 'escritorio.jpg', 'das'),
(9, 'grupo 9', 'tanque Respsol.jpg', 'el segundo'),
(10, 'grupo 10 creado', 'tanque Respsol.jpg', 'el segundo'),
(11, 'grupo 11', 'tanque Respsol.jpg', 'el segundo'),
(12, 'Los demas', 'baul.JPG', 'Grupos multiples '),
(13, 'd', '9cc632438ffbbe3fc1bf8bc64183a2ba.jpg', 'das'),
(14, 'd', '9cc632438ffbbe3fc1bf8bc64183a2ba1.jpg', 'das'),
(15, 'grupo nuevo', 'baul.JPG', 'este grupo es el mejol'),
(16, 'De roque', '9cc632438ffbbe3fc1bf8bc64183a2ba2.jpg', 'el grupo de roque'),
(17, 'f', '9cc632438ffbbe3fc1bf8bc64183a2ba3.jpg', 'f'),
(18, 'fa', '9cc632438ffbbe3fc1bf8bc64183a2ba4.jpg', 'f'),
(19, 'nuevo grupo', 'baul1.JPG', 'es un nuevo grupo'),
(20, 'sinfoto', 'default_grupo.png', 'grupo sin foto'),
(21, 'De roque sin foto', 'default_grupo.png', 'dkjls'),
(22, 'Grupete sin foto', 'default_grupo.png', 'grupete sin foto'),
(23, 'dd', 'default_grupo.png', 'das'),
(24, 'Grupete raro', 'default_grupo.png', 'es un grupete super raro'),
(25, 'Las panteras', 'default_grupo.png', 'Blancas'),
(26, 'Panteras dos', 'default_grupo.png', 'las panteras dos'),
(27, 'probar', 'default_grupo.png', 'lazos de familia'),
(28, 'picha corta', 'default_grupo.png', 'Aca estamos los picha corta'),
(29, 'Grupo novedoso', 'default_grupo.png', 'el nuevito de todos'),
(30, 'Pantera Rosa', 'escritorio1.jpg', 'Regay'),
(31, 'grupo 2020', 'baul3.JPG', 'el nuevo grupo'),
(32, 'Grupo dios', 'baul4.JPG', 'con nuevos parametros');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `grupo_comparte`
--

CREATE TABLE `grupo_comparte` (
  `id_grupo_fk` int(10) NOT NULL,
  `id_publicacion_fk` int(10) NOT NULL,
  `id_cuenta_fk` int(10) NOT NULL,
  `fecha_hora` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `grupo_comparte`
--

INSERT INTO `grupo_comparte` (`id_grupo_fk`, `id_publicacion_fk`, `id_cuenta_fk`, `fecha_hora`) VALUES
(1, 160, 24, '2020-05-18 02:58:15'),
(1, 161, 24, '2020-05-18 03:06:14'),
(1, 166, 24, '2020-05-18 22:44:46'),
(1, 175, 24, '2020-06-08 02:44:22'),
(1, 176, 24, '2020-06-09 03:39:02'),
(1, 179, 24, '2020-06-11 03:15:46'),
(1, 185, 24, '2020-06-11 23:49:52'),
(1, 187, 24, '2020-06-11 23:51:29'),
(1, 201, 24, '2020-06-16 21:52:50'),
(1, 202, 24, '2020-06-16 23:30:42'),
(1, 203, 24, '2020-06-16 23:30:42'),
(1, 204, 24, '2020-06-16 23:30:43'),
(1, 205, 24, '2020-06-16 23:30:43'),
(1, 206, 24, '2020-06-16 23:30:43'),
(1, 207, 24, '2020-06-16 23:30:43'),
(1, 211, 24, '2020-06-16 23:53:37'),
(1, 212, 24, '2020-06-16 23:55:07'),
(1, 213, 24, '2020-06-16 23:57:03'),
(1, 214, 24, '2020-06-17 00:13:19'),
(1, 219, 24, '2020-06-17 01:33:57'),
(1, 227, 24, '2020-06-17 02:10:04'),
(1, 230, 24, '2020-06-17 03:44:35'),
(1, 231, 24, '2020-06-17 03:45:40'),
(1, 232, 24, '2020-06-17 03:47:11'),
(1, 234, 24, '2020-06-17 03:51:52'),
(1, 235, 24, '2020-06-17 03:59:05'),
(1, 236, 24, '2020-06-17 04:04:56'),
(1, 237, 24, '2020-06-17 04:21:24'),
(1, 242, 24, '2020-06-17 17:15:04'),
(1, 243, 24, '2020-06-17 19:01:03'),
(1, 245, 24, '2020-06-17 19:34:41'),
(1, 292, 24, '2020-06-19 00:43:59'),
(2, 216, 24, '2020-06-17 01:19:24'),
(2, 218, 24, '2020-06-17 01:31:08'),
(2, 228, 24, '2020-06-17 02:32:51'),
(2, 229, 24, '2020-06-17 02:32:51'),
(2, 240, 24, '2020-06-17 16:54:51'),
(2, 241, 24, '2020-06-17 17:01:46'),
(8, 158, 24, '2020-05-18 02:40:05'),
(8, 223, 24, '2020-06-17 01:58:17'),
(8, 251, 24, '2020-06-18 00:03:24'),
(10, 215, 24, '2020-06-17 00:35:51'),
(12, 217, 24, '2020-06-17 01:30:15');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `gusta`
--

CREATE TABLE `gusta` (
  `id_cuenta_fk` int(10) NOT NULL,
  `id_publicacion_fk` int(10) NOT NULL,
  `fecha_hora` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `gusta`
--

INSERT INTO `gusta` (`id_cuenta_fk`, `id_publicacion_fk`, `fecha_hora`) VALUES
(9, 80, '2020-05-10 02:47:43'),
(9, 94, '2018-07-12 19:12:19'),
(9, 96, '2018-07-12 19:12:07'),
(9, 97, '2018-07-12 18:55:10'),
(9, 136, '2020-05-10 02:47:36'),
(9, 137, '2020-05-10 02:47:26'),
(9, 138, '2020-05-10 02:47:23'),
(9, 170, '2020-05-23 20:42:54'),
(9, 175, '2020-06-09 02:48:53'),
(15, 104, '2018-07-13 02:45:08'),
(15, 106, '2018-07-13 03:05:04'),
(24, 10, '2019-11-23 23:49:34'),
(24, 14, '2019-11-26 23:24:54'),
(24, 58, '2018-07-12 05:43:47'),
(24, 62, '2019-11-22 02:25:35'),
(24, 64, '2019-11-23 12:44:09'),
(24, 66, '2019-11-21 03:00:23'),
(24, 67, '2019-11-23 12:44:06'),
(24, 70, '2019-11-23 12:44:04'),
(24, 72, '2020-06-10 03:00:43'),
(24, 73, '2019-11-23 12:43:45'),
(24, 75, '2019-11-23 23:00:57'),
(24, 80, '2019-11-22 02:22:48'),
(24, 83, '2019-11-22 02:19:07'),
(24, 84, '2019-11-21 23:24:38'),
(24, 85, '2019-11-23 12:43:39'),
(24, 87, '2019-11-22 04:09:48'),
(24, 88, '2019-11-23 12:43:32'),
(24, 89, '2020-06-28 03:54:14'),
(24, 91, '2018-07-12 02:16:55'),
(24, 92, '2020-05-24 15:57:07'),
(24, 93, '2019-11-22 02:10:27'),
(24, 96, '2019-11-23 12:53:29'),
(24, 97, '2019-11-27 03:42:17'),
(24, 98, '2019-11-27 04:05:28'),
(24, 99, '2019-11-23 05:04:15'),
(24, 100, '2019-11-22 02:52:51'),
(24, 106, '2019-11-20 00:31:48'),
(24, 107, '2019-11-30 22:00:18'),
(24, 108, '2019-11-23 05:04:06'),
(24, 109, '2019-11-30 15:46:34'),
(24, 111, '2019-11-30 15:19:42'),
(24, 131, '2019-11-30 15:46:14'),
(24, 134, '2019-12-01 03:20:46'),
(24, 139, '2019-11-28 03:20:45'),
(24, 142, '2019-11-27 00:45:16'),
(24, 143, '2019-11-27 04:04:56'),
(24, 146, '2019-11-29 01:51:18'),
(24, 148, '2019-11-29 01:38:40'),
(24, 160, '2020-06-20 06:14:30'),
(24, 162, '2020-05-22 19:40:36'),
(24, 165, '2020-05-22 19:40:01'),
(24, 166, '2020-06-09 02:05:46'),
(24, 168, '2020-06-28 04:42:35'),
(24, 170, '2020-06-20 02:08:17'),
(24, 174, '2020-06-08 00:27:48'),
(24, 175, '2020-06-10 02:14:21'),
(24, 178, '2020-06-11 00:11:17'),
(24, 183, '2020-06-11 22:24:15'),
(24, 191, '2020-06-20 02:28:21'),
(24, 192, '2020-06-20 02:28:38'),
(24, 194, '2020-06-28 04:42:51'),
(24, 197, '2020-06-20 02:34:40'),
(24, 202, '2020-06-20 02:34:28'),
(24, 204, '2020-06-20 02:26:42'),
(24, 209, '2020-06-28 04:27:35'),
(24, 215, '2020-06-17 01:15:48'),
(24, 224, '2020-06-20 02:28:12'),
(24, 225, '2020-06-17 02:07:50'),
(24, 228, '2020-06-20 02:26:35'),
(24, 231, '2020-06-20 02:28:08'),
(24, 234, '2020-06-20 02:28:05'),
(24, 237, '2020-06-20 02:28:03'),
(24, 239, '2020-06-17 05:13:38'),
(24, 244, '2020-06-28 04:26:50'),
(24, 247, '2020-06-28 04:26:46'),
(24, 250, '2020-06-28 04:26:42'),
(24, 253, '2020-06-18 00:06:57'),
(24, 263, '2020-06-28 04:26:33'),
(24, 268, '2020-06-20 00:17:35'),
(24, 270, '2020-06-28 03:56:40'),
(24, 272, '2020-06-20 02:09:06'),
(24, 276, '2020-06-20 02:26:02'),
(24, 281, '2020-06-28 04:20:08'),
(24, 282, '2020-06-28 04:24:11'),
(24, 283, '2020-06-28 04:24:12'),
(24, 286, '2020-06-28 02:11:58'),
(24, 287, '2020-06-28 04:26:09'),
(24, 292, '2020-06-20 02:09:30'),
(24, 293, '2020-06-28 03:56:31'),
(24, 296, '2020-06-28 04:24:57'),
(24, 297, '2020-06-28 04:26:00'),
(24, 298, '2020-06-20 06:14:26'),
(24, 300, '2020-06-28 04:24:51'),
(24, 304, '2020-06-28 04:43:05'),
(28, 108, '2018-07-13 04:59:07'),
(39, 18, '2018-07-13 22:08:36');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `notifica`
--

CREATE TABLE `notifica` (
  `id_notifica` int(10) NOT NULL,
  `id_de_fk` int(10) NOT NULL,
  `id_para_fk` int(10) NOT NULL,
  `id_publicacion_fk` int(10) DEFAULT NULL,
  `mensaje` varchar(200) NOT NULL,
  `estado` tinyint(1) NOT NULL,
  `fecha` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `notifica`
--

INSERT INTO `notifica` (`id_notifica`, `id_de_fk`, `id_para_fk`, `id_publicacion_fk`, `mensaje`, `estado`, `fecha`) VALUES
(1, 24, 24, NULL, 'dio Like', 1, '2020-05-22 19:40:01'),
(11, 24, 24, NULL, 'like', 1, '2020-05-23 21:16:42'),
(12, 24, 24, NULL, 'like', 1, '2020-05-23 21:16:51'),
(36, 24, 24, NULL, 'Le gusta tu publicacion', 1, '2020-05-24 00:19:13'),
(37, 24, 24, NULL, 'Le gusta tu publicacion', 1, '2020-05-24 00:19:50'),
(38, 24, 24, 170, 'Le gusta tu publicacion', 1, '2020-05-24 00:21:35'),
(39, 24, 24, 169, 'Le gusta tu publicacion', 1, '2020-05-24 00:33:34'),
(40, 24, 24, 170, 'Le gusta tu publicacion', 1, '2020-05-24 00:33:55'),
(41, 24, 24, 170, 'le gusta tu publicacion', 1, '2020-05-24 00:39:30'),
(42, 24, 24, 170, 'le gusta tu publicacion', 1, '2020-05-24 03:15:26'),
(43, 24, 24, 170, 'le gusta tu publicacion', 1, '2020-05-24 03:15:37'),
(44, 24, 24, 170, 'le gusta tu publicacion', 1, '2020-05-24 03:16:00'),
(45, 24, 24, 170, 'le gusta tu publicacion', 1, '2020-05-24 03:16:15'),
(46, 24, 24, 170, 'le gusta tu publicacion', 1, '2020-05-24 03:16:18'),
(47, 24, 24, 170, 'le gusta tu publicacion', 1, '2020-05-24 03:16:21'),
(48, 24, 24, 170, 'le gusta tu publicacion', 1, '2020-05-24 03:16:23');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pagina`
--

CREATE TABLE `pagina` (
  `id_cuenta_fk` int(10) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `fec_inaugura` date NOT NULL,
  `pag_web` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `pagina`
--

INSERT INTO `pagina` (`id_cuenta_fk`, `nombre`, `fec_inaugura`, `pag_web`) VALUES
(37, 'LaWeb', '1111-11-11', 'noweb'),
(38, 'pagina', '2018-07-03', 'laweb.com.uy'),
(41, 'pagina', '2000-11-11', 'www'),
(42, 'pagina', '2017-07-03', 'web'),
(45, 'SuperWeb', '2010-12-12', 'panueva'),
(47, 'Pato', '2001-02-12', 'panueva'),
(54, 'ricardo', '2017-07-02', 'dasdfasd'),
(55, 'ricardo', '2017-07-02', 'dasdfasd'),
(56, 'ricardo', '2019-07-18', 'uigog'),
(58, 'pagin_nueva', '2019-11-11', 'pagina_nueva.com');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `publicacion`
--

CREATE TABLE `publicacion` (
  `id_publicacion` int(10) NOT NULL,
  `cont_post` varchar(1500) NOT NULL,
  `type_post` tinyint(1) NOT NULL,
  `nom_img` varchar(200) DEFAULT NULL,
  `link_video` varchar(20) DEFAULT NULL,
  `nom_album` varchar(20) DEFAULT NULL,
  `estado` int(1) DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `publicacion`
--

INSERT INTO `publicacion` (`id_publicacion`, `cont_post`, `type_post`, `nom_img`, `link_video`, `nom_album`, `estado`) VALUES
(3, 'prueba2', 1, NULL, NULL, NULL, 1),
(4, 'Quiero compartir esto tan bello :P', 1, NULL, NULL, NULL, 1),
(5, 'Nuevo compartio\r\n', 1, NULL, NULL, NULL, 1),
(6, 'Compartido 12:15', 1, NULL, NULL, NULL, 1),
(7, 'Quiero compartir esto conmigo mismo ya que no tengo amigos', 1, NULL, NULL, NULL, 1),
(8, 'Ahora ya tengo amigos\r\n', 1, NULL, NULL, NULL, 1),
(9, 'Compartido', 1, NULL, NULL, NULL, 1),
(10, 'fjhoadsjoasdj', 1, NULL, NULL, NULL, 1),
(11, 'Que bello dia', 1, NULL, NULL, NULL, 1),
(12, 'muy bello\r\n', 1, NULL, NULL, NULL, 1),
(13, 'Compartido ', 1, NULL, NULL, NULL, 1),
(14, 'sss', 1, NULL, NULL, NULL, 1),
(15, 'hola', 1, NULL, NULL, NULL, 1),
(16, 'hola2', 1, NULL, NULL, NULL, 1),
(17, 'Prueba', 1, NULL, NULL, NULL, 1),
(18, 'Yupi', 1, NULL, NULL, NULL, 1),
(19, 'hola\r\n', 1, NULL, NULL, NULL, 1),
(20, 'otro\r\n', 1, NULL, NULL, NULL, 1),
(21, 'Editado raro<br>', 1, NULL, NULL, NULL, 1),
(22, 'aaaa', 1, NULL, NULL, NULL, 1),
(23, 'comparti desde muro', 1, NULL, NULL, NULL, 1),
(24, 'fff', 1, NULL, NULL, NULL, 1),
(25, 'Compartido desde muro Gral', 1, NULL, NULL, NULL, 1),
(26, 'model\r\n', 1, NULL, NULL, NULL, 1),
(27, 'prueba', 1, NULL, NULL, NULL, 1),
(28, 'compartir muro gral', 1, NULL, NULL, NULL, 1),
(29, 'compartido ahora', 1, NULL, NULL, NULL, 1),
(30, 'Nuevo compartido', 1, NULL, NULL, NULL, 1),
(31, 'hola', 1, NULL, NULL, NULL, 1),
(32, 'Soy Ganzo, hola a Pega amio', 1, NULL, NULL, NULL, 1),
(33, 'jajaja pega\r\n', 1, NULL, NULL, NULL, 1),
(34, 'Muro compartido', 1, NULL, NULL, NULL, 1),
(35, 'Prueba domingo', 1, NULL, NULL, NULL, 1),
(36, 'comparto', 1, NULL, NULL, NULL, 1),
(37, 'Prueba general\r\n', 1, NULL, NULL, NULL, 1),
(38, 'prueba 17 00', 1, NULL, NULL, NULL, 1),
(39, 'hsdñahfñlkjsd', 1, NULL, NULL, NULL, 1),
(40, 'gadfs', 1, NULL, NULL, NULL, 1),
(41, '1', 1, NULL, NULL, NULL, 1),
(42, '31', 1, NULL, NULL, NULL, 1),
(43, 'cosa de live\r\n', 1, NULL, NULL, NULL, 1),
(44, 'Compartir', 1, NULL, NULL, NULL, 1),
(45, 'fdas', 1, NULL, NULL, NULL, 1),
(46, 'fdff', 1, NULL, NULL, NULL, 1),
(47, 'gelou', 1, NULL, NULL, NULL, 1),
(48, 'Del tipo 1', 1, NULL, NULL, NULL, 1),
(49, 'Antes del', 1, NULL, NULL, NULL, 1),
(50, 'foto cuadrada 5', 1, NULL, NULL, NULL, 1),
(51, 'nada', 1, NULL, NULL, NULL, 1),
(52, '  ', 1, NULL, NULL, NULL, 1),
(53, 'solo por probar', 1, NULL, NULL, NULL, 1),
(54, 'probando', 1, NULL, NULL, NULL, 1),
(55, 'Antes del bebedero?v=antes_del_bebedero7.png', 2, 'antes_del_bebedero3.png', NULL, NULL, 1),
(56, 'Que huevoo!!?v=insp-cuadradas-122.jpg', 2, 'antes_del_bebedero3.png', NULL, NULL, 1),
(57, 'Soy el burro de Shreck', 1, NULL, NULL, NULL, 1),
(58, 'pongo pa romper ?v=lorompo?v=insp-cuadradas-97.jpg', 2, 'insp-cuadradas-54.jpg', NULL, NULL, 1),
(59, 'pongo pa romper ?v=lorompo', 1, NULL, NULL, NULL, 1),
(60, 'romper ?v=ggg?v=insp-cuadradas-55.jpg', 2, 'antes_del_bebedero3.png', NULL, NULL, 1),
(61, 'para romeprlo todo ?v= horana?v=insp-cuadradas-98.jpg', 2, 'antes_del_bebedero3.png', NULL, NULL, 1),
(62, 'lo rompo ?v= roto?v=insp-cuadradas-123.jpg', 2, 'insp-cuadradas-54.jpg', NULL, NULL, 1),
(63, 'nada?v=insp-cuadradas-56.jpg', 1, '', NULL, NULL, 1),
(64, 'antes?v=antes_del_bebedero8.png', 2, 'antes_del_bebedero8.png', NULL, NULL, 1),
(65, 'Hola', 1, NULL, NULL, NULL, 1),
(66, 'Hola desde muro', 1, NULL, NULL, NULL, 1),
(67, 'Hola Nuevamenta', 1, NULL, NULL, NULL, 1),
(68, 'ddd', 1, NULL, NULL, NULL, 1),
(69, 'fff', 1, NULL, NULL, NULL, 1),
(70, 'como no', 1, NULL, NULL, NULL, 1),
(71, '', 2, 'insp-cuadradas-124.jpg', NULL, NULL, 1),
(72, '', 2, 'insp-cuadradas-57.jpg', NULL, NULL, 1),
(73, 'Californication', 3, NULL, 'YlUKcNNmywk', NULL, 1),
(74, '', 3, NULL, 'YlUKcNNmywk', NULL, 1),
(75, 'Nirvana', 3, NULL, 'hTWKbfoikeg', NULL, 1),
(76, 'Amy', 3, NULL, 'TJAfLE39ZZ8', NULL, 1),
(77, 'El Zeppelin', 3, NULL, 'xbhCPt6PZIU', NULL, 1),
(78, 'Prueba subir archivo', 1, NULL, NULL, NULL, 1),
(79, 'Nueva publicacion', 1, NULL, NULL, NULL, 1),
(80, 'Estopa\r\n\r\n', 3, NULL, 'wECwsE4yNSQ', NULL, 1),
(81, 'rrr', 1, NULL, NULL, NULL, 1),
(82, 'Probando', 1, NULL, NULL, NULL, 1),
(83, 'Compartido desde muro general', 1, NULL, NULL, NULL, 1),
(84, 'Prueba', 1, NULL, NULL, NULL, 1),
(85, '', 2, 'insp-cuadradas-99.jpg', NULL, NULL, 1),
(86, '', 2, 'insp-cuadradas-59.jpg', NULL, NULL, 1),
(87, '', 2, 'insp-cuadradas-125.jpg', NULL, NULL, 1),
(88, 'muro nueva version', 1, NULL, NULL, NULL, 1),
(89, '', 3, NULL, 'YlUKcNNmywk', NULL, 1),
(90, 'fafa', 1, NULL, NULL, NULL, 1),
(91, '', 3, NULL, NULL, NULL, 1),
(92, '', 3, NULL, 'gr3JVzmWkv0', NULL, 1),
(93, 'Hola', 1, NULL, NULL, NULL, 1),
(94, '', 2, 'insp-cuadradas-510.jpg', NULL, NULL, 1),
(95, '', 2, 'insp-cuadradas-910.jpg', NULL, NULL, 1),
(96, 'Que huevo', 2, 'insp-cuadradas-127.jpg', NULL, NULL, 1),
(97, '<span style=\"color: rgb(255, 0, 0);\">RED</span> HOT<br>', 3, NULL, 'YlUKcNNmywk', NULL, 1),
(98, '', 2, 'insp-cuadradas-911.jpg', '', NULL, 1),
(99, 'que linda bici', 2, 'insp-cuadradas-912.jpg', NULL, NULL, 1),
(100, '', 2, 'insp-cuadradas-913.jpg', NULL, NULL, 1),
(101, 'Eres una chica muy bonita', 3, NULL, 'HgsgRH-K1OA', NULL, 1),
(102, 'compartodo en mi muro ', 1, NULL, NULL, NULL, 1),
(103, '', 2, 'detalles.gif', NULL, NULL, 1),
(104, '', 2, 'antes_del_bebedero13.png', NULL, NULL, 1),
(105, '', 2, 'insp-cuadradas-915.jpg', NULL, NULL, 1),
(106, '', 2, 'insp-cuadradas-128.jpg', NULL, NULL, 1),
(107, 'Probando', 1, NULL, NULL, NULL, 1),
(108, 'otra prueba', 1, NULL, NULL, NULL, 1),
(109, 'prueba', 1, NULL, NULL, NULL, 1),
(110, '', 3, NULL, 'QfAQCEnRXww', NULL, 1),
(111, 'compartido', 1, NULL, NULL, NULL, 1),
(112, 'war', 2, 'war_01.jpg', NULL, NULL, 1),
(113, 'war02', 2, 'war_02.jpg', NULL, NULL, 1),
(114, 'war03', 2, 'war_03.jpg', NULL, NULL, 1),
(115, 'callofduty01', 2, 'callofduty_01.jpg', NULL, NULL, 1),
(116, 'callofduty02', 2, 'callofduty_02.jpg', NULL, NULL, 1),
(117, 'fifa01', 2, 'fifa_01.jpg', NULL, NULL, 1),
(118, 'fifa02', 2, 'fifa_02.jpg', NULL, NULL, 1),
(119, 'fifa03', 2, 'fifa_03.jpg', NULL, NULL, 1),
(120, 'fifa04', 2, 'fifa_04.jpg', NULL, NULL, 1),
(121, 'fifa05', 2, 'fifa_05.jpg', NULL, NULL, 1),
(122, 'mortalkombat01', 2, 'mortalkombat_01.jpg', NULL, NULL, 1),
(123, '<span style=\"color: rgb(204, 204, 204);\">ss<span style=\"font-weight: bold;\">sss</span></span><br>', 2, 'insp-cuadradas-129.jpg', NULL, NULL, 1),
(124, '', 2, 'insp-cuadradas-10.jpg', NULL, NULL, 1),
(125, 'prueba', 1, NULL, NULL, NULL, 1),
(126, 'prueba', 1, NULL, NULL, NULL, 1),
(127, 'prueba', 1, NULL, NULL, NULL, 1),
(128, 'prueba', 1, NULL, NULL, NULL, 1),
(129, '', 1, NULL, NULL, NULL, 1),
(130, 'el niño vampiro<br>', 3, NULL, 'wECwsE4yNSQ', NULL, 1),
(131, '', 3, NULL, 'mzJj5-lubeM', NULL, 1),
(132, '', 2, 'insp-cuadradas-111.jpg', NULL, NULL, 1),
(133, 'Hola\r\n', 1, NULL, NULL, NULL, 1),
(134, '', 1, NULL, NULL, NULL, 1),
(135, '', 1, NULL, NULL, NULL, 1),
(136, '', 1, NULL, NULL, NULL, 1),
(137, '<span style=\"text-decoration: line-through;\">Los sonidos del silencio</span><br>', 1, NULL, NULL, NULL, 1),
(138, '<span style=\"font-style: italic;\">Esto está publi<span style=\"color: rgb(0, 255, 0);\">cado</span></span><br>', 2, 'pb22c1.jpg', NULL, NULL, 1),
(139, 'resd', 1, NULL, NULL, NULL, 0),
(140, '<span style=\"font-weight: bold;\">Negrita</span> <span style=\"font-style: italic;\">torcida <span style=\"font-weight: bold;\">Negrita Y torcida</span></span><span style=\"font-weight: bold;\"></span><br>', 1, NULL, NULL, NULL, 0),
(141, 'Esto<span style=\"font-weight: bold;\"> se ha editado</span><br>', 1, NULL, NULL, NULL, 0),
(142, 'Editado <span style=\"color: rgb(255, 0, 0);\">n<span style=\"color: rgb(102, 102, 102);\">ue</span>vam<span style=\"color: rgb(153, 153, 153);\">ente </span></span><br>', 1, NULL, NULL, NULL, 0),
(143, '<div>Holis</div>', 1, NULL, NULL, NULL, 0),
(144, '<div><span style=\"font-style: italic;\"><span style=\"color: rgb(69, 129, 142);\">probar</span></span></div><div><br></div>', 1, NULL, NULL, NULL, 0),
(145, '', 2, 'insp-cuadradas-531.jpg', NULL, NULL, 0),
(146, 'inserta foto', 2, 'insp-cuadradas-532.jpg', NULL, NULL, 0),
(147, '<div>Zambayonny</div><div><br></div>', 3, NULL, 'C5YGEZJal6U', NULL, 0),
(148, 'za', 3, NULL, 'C5YGEZJal6U', NULL, 0),
(149, '<p>Publico desde pantera Rosa</p>\r\n', 1, NULL, NULL, NULL, 1),
(150, '<p>hola</p>\r\n', 1, NULL, NULL, NULL, 1),
(151, '<p>morite</p>\r\n', 1, NULL, NULL, NULL, 1),
(152, '<p><span style=\"display:none\">&nbsp;</span>dd</p>\r\n', 1, NULL, NULL, NULL, 1),
(153, '<p>muro general pero de grupo</p>\r\n', 1, NULL, NULL, NULL, 1),
(154, '<p><span style=\"color:#c0392b\">Desde muro de grupo id_grupo -1</span></p>\r\n', 1, NULL, NULL, NULL, 1),
(156, '<p>es</p>\r\n', 1, NULL, NULL, NULL, 1),
(157, '<p>hola en grupo</p>\r\n', 1, NULL, NULL, NULL, 1),
(158, '<p>Hola grupo 2 estoy en la PILE</p>\r\n', 4, NULL, NULL, NULL, 1),
(159, '<p>muro</p>\r\n', 4, NULL, NULL, NULL, 1),
(160, '<p>desde muro 2</p>\r\n', 4, NULL, NULL, NULL, 1),
(161, '<p>compartido en grupo 8</p>\r\n', 4, NULL, NULL, NULL, 1),
(162, '<p>estoy en muro con el nuevo controlador</p>\r\n', 4, NULL, NULL, NULL, 1),
(163, '<p>ahora estoy en muro</p>\r\n\r\n<p>&nbsp;</p>\r\n', 1, NULL, NULL, NULL, 1),
(164, '<p>HOL<span style=\"color:#2ecc71\">A CHIC</span><span style=\"color:#e74c3c\">O</span>S</p>\r\n', 1, NULL, NULL, NULL, 1),
(165, '<p>55555</p>\r\n', 1, NULL, NULL, NULL, 1),
(166, '<p>putos todos los del grupo 1</p>\r\n', 4, NULL, NULL, NULL, 1),
(167, '<p>ba&ntilde;o</p>\r\n', 2, 'Baños-920.jpg', NULL, NULL, 1),
(168, '', 3, NULL, 'wECwsE4yNSQ', NULL, 1),
(169, '<p>ajedrez</p>\r\n', 3, NULL, 'lJoZkLBzB3s', NULL, 1),
(170, '<p>por la boca <img alt=\"wink\" height=\"23\" src=\"http://[::1]/gameover-frontend/public_html/assets/ckeditor/plugins/smiley/images/wink_smile.png\" title=\"wink\" width=\"23\" /></p>\r\n', 3, NULL, 'iUXs4Nt3Y7Y', NULL, 1),
(171, '', 2, 'escritorio1.jpg', NULL, NULL, 1),
(172, '', 2, 'escritorio2.jpg', NULL, NULL, 1),
(173, '', 2, 'pb11a3.jpg', NULL, NULL, 1),
(174, '<p>compartir e</p>\r\n', 1, NULL, NULL, NULL, 1),
(175, '<p>7/6/2020 nueva publicacion</p>\r\n', 4, NULL, NULL, NULL, 1),
(176, '<p>holis</p>\r\n', 4, NULL, NULL, NULL, 0),
(177, '<p>nueva 10/6/20</p>\r\n', 1, NULL, NULL, NULL, 1),
(178, '', 2, 'baul.JPG', NULL, NULL, 1),
(179, '<p>Hola estoy en grupo 2</p>\r\n', 4, NULL, NULL, NULL, 1),
(180, '<p>Hola en grupo 1 yo burro</p>\r\n', 4, NULL, NULL, NULL, 1),
(181, '<p>Foto</p>\r\n', 2, 'escritorio3.jpg', NULL, NULL, 1),
(182, '<p>hi tec</p>\r\n', 1, NULL, NULL, NULL, 1),
(183, '<p>en muro</p>\r\n', 1, NULL, NULL, NULL, 0),
(184, '<p><s>soooooos</s></p>\r\n', 1, NULL, NULL, NULL, 1),
(185, '<p>compartido muro general</p>\r\n', 4, NULL, NULL, NULL, 1),
(186, '<p>compartido grupo 1</p>\r\n', 1, NULL, NULL, NULL, 1),
(187, '<p>compartido en muro general</p>\r\n', 4, NULL, NULL, NULL, 1),
(188, '<p>compartido desde muro</p>\r\n\r\n<p>&nbsp;</p>\r\n', 1, NULL, NULL, NULL, 1),
(189, '<p>Compartido en muro 2</p>\r\n', 1, NULL, NULL, NULL, 1),
(190, '<p>compartido en grupo 1</p>\r\n', 1, NULL, NULL, NULL, 1),
(191, '<p>fin grupo 1</p>\r\n', 1, NULL, NULL, NULL, 1),
(192, '<p>compartido en grupo <span style=\"background-color:#2ecc71\">1</span></p>\r\n', 1, NULL, NULL, NULL, 1),
(193, '<p>da</p>\r\n', 1, NULL, NULL, NULL, 1),
(194, '<p>si est&aacute; de mas</p>\r\n', 1, NULL, NULL, NULL, 1),
(195, '<p>por</p>\r\n', 1, NULL, NULL, NULL, 1),
(196, '<p>compartido hoy</p>\r\n', 1, NULL, NULL, NULL, 1),
(197, '<p>con this-&gt;input-&gt;get</p>\r\n', 1, NULL, NULL, NULL, 1),
(198, '<p>d</p>\r\n', 1, NULL, NULL, NULL, 1),
(199, 'post', 1, NULL, NULL, NULL, 1),
(200, 'post', 1, NULL, NULL, NULL, 1),
(201, 'desdegrupo1', 4, NULL, NULL, NULL, 1),
(202, '<p>dfas</p>\r\n', 4, NULL, NULL, NULL, 1),
(203, '<p>dfas</p>\r\n', 4, NULL, NULL, NULL, 1),
(204, '<p>dfas</p>\r\n', 4, NULL, NULL, NULL, 1),
(205, '<p>dfas</p>\r\n', 4, NULL, NULL, NULL, 1),
(206, '<p>dfas</p>\r\n', 4, NULL, NULL, NULL, 1),
(207, '<p>dfas</p>\r\n', 4, NULL, NULL, NULL, 1),
(208, '<p>fasd</p>\r\n', 1, NULL, NULL, NULL, 1),
(209, '<p>fasd</p>\r\n', 1, NULL, NULL, NULL, 1),
(210, '<p>fdsa</p>\r\n', 1, NULL, NULL, NULL, 1),
(211, '<p>grupo</p>\r\n', 4, NULL, NULL, NULL, 1),
(212, '<p>compartir desde grupo 2</p>\r\n', 4, NULL, NULL, NULL, 1),
(213, '<p>compartir desde grupo 2</p>\r\n', 4, NULL, NULL, NULL, 1),
(214, '<p>compartir desde grupo 10</p>\r\n', 4, NULL, NULL, NULL, 1),
(215, '<p>Desde Grupo 10</p>\r\n', 4, NULL, NULL, NULL, 1),
(216, '<p>Publicado en grupo 2 funcionando</p>\r\n', 4, NULL, NULL, NULL, 1),
(217, '<p>Los demas</p>\r\n', 4, NULL, NULL, NULL, 1),
(218, '<p>el grupo 2</p>\r\n', 4, NULL, NULL, NULL, 1),
(219, '<p>Controler Muro</p>\r\n', 4, NULL, NULL, NULL, 1),
(220, '<p>comparto en mi muro</p>\r\n', 1, NULL, NULL, NULL, 1),
(221, '<p>Escritorio</p>\r\n', 2, 'escritorio4.jpg', NULL, NULL, 1),
(222, '<p>por la boca vive el pe</p>\r\n', 3, NULL, 'iUXs4Nt3Y7Y', NULL, 1),
(223, '<p>grupo sds 8</p>\r\n', 4, NULL, NULL, NULL, 1),
(224, '<p>comparti d<span style=\"color:#1abc9c\">esde grupo gral. </span></p>\r\n', 1, NULL, NULL, NULL, 1),
(225, '<p>comparti d<span style=\"color:#1abc9c\">esde grupo gral. </span></p>\r\n', 1, NULL, NULL, NULL, 1),
(226, '<p>Baul</p>\r\n', 2, 'baul1.JPG', NULL, NULL, 1),
(227, '<p>nueva prueba g1</p>\r\n', 4, NULL, NULL, NULL, 1),
(228, '<p>canoa</p>\r\n', 4, NULL, NULL, NULL, 1),
(229, '<p>canoa</p>\r\n', 4, NULL, NULL, NULL, 1),
(230, '<p>pdef m1</p>\r\n', 4, NULL, NULL, NULL, 1),
(231, '<p>pdef m1 de nuevo</p>\r\n', 4, NULL, NULL, NULL, 1),
(232, '<p>calmao</p>\r\n', 4, NULL, NULL, NULL, 1),
(233, '<p>ds</p>\r\n', 1, NULL, NULL, NULL, 1),
(234, '<p>muro 1</p>\r\n', 4, NULL, NULL, NULL, 1),
(235, '<p>cargar mas datos</p>\r\n', 4, NULL, NULL, NULL, 1),
(236, '<p>dsddd4</p>\r\n', 4, NULL, NULL, NULL, 1),
(237, '<p>d v<span style=\"color:#1abc9c\">vv</span></p>\r\n', 4, NULL, NULL, NULL, 1),
(238, '<p>hgfhh<span style=\"color:#e74c3c\">hhhh</span></p>\r\n', 1, NULL, NULL, NULL, 1),
(239, '<p>hgfhh<span style=\"color:#e74c3c\">hhhh</span></p>\r\n', 1, NULL, NULL, NULL, 1),
(240, '<p>en el grupo 2</p>\r\n', 4, NULL, NULL, NULL, 1),
(241, '<p>grupo 2</p>\r\n', 4, NULL, NULL, NULL, 1),
(242, '<p>guno</p>\r\n', 4, NULL, NULL, NULL, 1),
(243, '<p>PRUEBA</p>\r\n', 4, NULL, NULL, NULL, 1),
(244, '<p>Muro general</p>\r\n', 1, NULL, NULL, NULL, 1),
(245, '<p>Muro Grupo 1</p>\r\n', 4, NULL, NULL, NULL, 1),
(246, '<p>d</p>\r\n', 4, NULL, NULL, NULL, 1),
(247, '<p>r</p>\r\n', 1, NULL, NULL, NULL, 1),
(248, '<p>ds</p>\r\n', 4, NULL, NULL, NULL, 1),
(249, '', 2, '9cc632438ffbbe3fc1bf8bc64183a2ba.jpg', NULL, NULL, 1),
(250, '<p>Money</p>\r\n', 3, NULL, 'wTP2RUD_cL0', NULL, 1),
(251, '<p>FADS que grupo ma lindo</p>\r\n', 4, NULL, NULL, NULL, 1),
(252, '<p>ddsdsd</p>\r\n', 1, NULL, NULL, NULL, 1),
(253, '<p>ggd</p>\r\n', 1, NULL, NULL, NULL, 1),
(254, '<p>muro</p>\r\n', 1, NULL, NULL, NULL, 1),
(255, '<p>desde muro</p>\r\n', 1, NULL, NULL, NULL, 1),
(256, '<p>respuesta</p>\r\n', 1, NULL, NULL, NULL, 1),
(257, '<p>ff</p>\r\n', 1, NULL, NULL, NULL, 1),
(258, '<p>ff</p>\r\n', 1, NULL, NULL, NULL, 1),
(259, '<p>ff</p>\r\n', 1, NULL, NULL, NULL, 1),
(260, '<p>dfdsa</p>\r\n', 1, NULL, NULL, NULL, 1),
(261, '<p>enviado a muro</p>\r\n', 1, NULL, NULL, NULL, 1),
(262, '<p>hfd</p>\r\n', 1, NULL, NULL, NULL, 1),
(263, '<p>dsafdf</p>\r\n', 1, NULL, NULL, NULL, 1),
(264, '<p>fdsa</p>\r\n', 1, NULL, NULL, NULL, 1),
(265, '<p>dfdasdfs</p>\r\n', 1, NULL, NULL, NULL, 1),
(266, '<p>fdsa</p>\r\n', 1, NULL, NULL, NULL, 1),
(267, '<p>dsafds</p>\r\n', 1, NULL, NULL, NULL, 1),
(268, '<p>dsafds</p>\r\n', 1, NULL, NULL, NULL, 1),
(269, '<p>dsd</p>\r\n', 1, NULL, NULL, NULL, 1),
(270, '<p>7hh</p>\r\n', 1, NULL, NULL, NULL, 1),
(271, '<p>ddfd</p>\r\n', 1, NULL, NULL, NULL, 1),
(272, '<p>estoy en opera</p>\r\n', 1, NULL, NULL, NULL, 1),
(273, '<p>Saco de la cabeza</p>\r\n', 1, NULL, NULL, NULL, 1),
(274, '<p>vez un millon</p>\r\n', 1, NULL, NULL, NULL, 1),
(275, '<p>yyttttt</p>\r\n', 1, NULL, NULL, NULL, 1),
(276, '<p><img alt=\"smiley\" height=\"23\" src=\"http://localhost/gameover-frontend/public_html/assets/ckeditor/plugins/smiley/images/regular_smile.png\" title=\"smiley\" width=\"23\" /></p>\r\n', 1, NULL, NULL, NULL, 1),
(277, '<p>txt c</p>\r\n', 1, NULL, NULL, NULL, 1),
(278, '<p>hacia muro</p>\r\n', 1, NULL, NULL, NULL, 1),
(279, '<p>foto</p>\r\n', 2, 'baul2.JPG', NULL, NULL, 1),
(280, '<p>compartir</p>\r\n', 1, NULL, NULL, NULL, 1),
(281, '<p>esito</p>\r\n', 1, NULL, NULL, NULL, 1),
(282, '<p>es compartir</p>\r\n', 1, NULL, NULL, NULL, 1),
(283, '<p>e comparit</p>\r\n', 1, NULL, NULL, NULL, 1),
(284, '<p>es comdjjd</p>\r\n', 1, NULL, NULL, NULL, 1),
(285, '<p>ultimo test</p>\r\n', 1, NULL, NULL, NULL, 1),
(286, '<p>die</p>\r\n', 1, NULL, NULL, NULL, 1),
(287, '<p>diez</p>\r\n', 1, NULL, NULL, NULL, 1),
(288, '<p>si recarga no me importa</p>\r\n', 1, NULL, NULL, NULL, 1),
(289, '<p>ajax</p>\r\n', 1, NULL, NULL, NULL, 1),
(290, '<p>ala</p>\r\n', 1, NULL, NULL, NULL, 1),
(291, '<p>compartir</p>\r\n', 1, NULL, NULL, NULL, 1),
(292, 'grupo 1 ahora', 4, NULL, NULL, NULL, 1),
(293, '', 2, 'escritorio5.jpg', NULL, NULL, 1),
(294, '<p>nuevo post</p>\r\n', 1, NULL, NULL, NULL, 1),
(295, '<p>Compartido en opera</p>\r\n', 1, NULL, NULL, NULL, 1),
(296, '', 2, 'escritorio6.jpg', NULL, NULL, 1),
(297, '', 2, 'escritorio7.jpg', NULL, NULL, 1),
(298, '<p>Estoy en el grupo uno</p>\r\n', 4, NULL, NULL, NULL, 1),
(299, '<p>publicacion</p>\r\n', 1, NULL, NULL, NULL, 1),
(300, '<p>cambiado</p>\r\n', 1, NULL, NULL, NULL, 1),
(303, '<p>prueba usar sin amigos</p>\r\n', 1, NULL, NULL, NULL, 1),
(304, '', 2, 'escritorio8.jpg', NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `publicidad`
--

CREATE TABLE `publicidad` (
  `id_publicidad` int(10) NOT NULL,
  `nom_publi` varchar(100) NOT NULL,
  `link_web` varchar(100) NOT NULL,
  `fecha_ini` timestamp NOT NULL DEFAULT current_timestamp(),
  `edad_min` tinyint(4) NOT NULL DEFAULT 0,
  `edad_max` tinyint(4) NOT NULL DEFAULT 0,
  `segmento_geo` varchar(100) DEFAULT NULL,
  `genero` tinyint(4) DEFAULT NULL,
  `estado` tinyint(4) NOT NULL,
  `contador` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `publicidad`
--

INSERT INTO `publicidad` (`id_publicidad`, `nom_publi`, `link_web`, `fecha_ini`, `edad_min`, `edad_max`, `segmento_geo`, `genero`, `estado`, `contador`) VALUES
(1, 'COCA-COLA.png', 'htttp://cocacola.com', '2020-06-26 18:49:41', 0, 0, NULL, NULL, 1, 0),
(2, 'mas_18.jpg', 'http://mayordeedad.com', '2020-06-26 18:49:41', 18, 0, NULL, NULL, 1, 0),
(3, 'solo_chicas.jpg', 'http://mujeres.com', '2020-06-26 18:49:41', 0, 0, NULL, 1, 1, 100),
(4, 'solo_chicos.png', 'http://hombres.com', '2020-06-26 18:49:41', 0, 23, NULL, 2, 2, 0),
(5, 'halo-infinite.jpg', 'http://www.halo.com.prueba', '2020-06-28 01:10:58', 0, 0, NULL, NULL, 1, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sigue`
--

CREATE TABLE `sigue` (
  `id_cuenta_p` int(10) NOT NULL,
  `id_cuenta_u` int(10) NOT NULL,
  `fecha_ini` timestamp NOT NULL DEFAULT current_timestamp(),
  `fecha_fin` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `suscripcion`
--

CREATE TABLE `suscripcion` (
  `id_suscrip` int(10) NOT NULL,
  `id_cuenta_fk` int(10) NOT NULL,
  `type` tinyint(1) NOT NULL,
  `fecha_ini` timestamp NOT NULL DEFAULT current_timestamp(),
  `fecha_fin` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `suscripcion`
--

INSERT INTO `suscripcion` (`id_suscrip`, `id_cuenta_fk`, `type`, `fecha_ini`, `fecha_fin`) VALUES
(1, 42, 2, '2018-07-19 03:11:42', '2019-07-20 09:36:30'),
(2, 45, 0, '2019-08-31 16:54:47', '0000-00-00 00:00:00'),
(3, 47, 0, '2019-08-31 17:18:00', '0000-00-00 00:00:00'),
(4, 54, 0, '2019-09-01 03:24:55', '0000-00-00 00:00:00'),
(5, 55, 0, '2019-09-01 03:28:59', '0000-00-00 00:00:00'),
(6, 56, 0, '2019-09-01 03:47:23', '0000-00-00 00:00:00'),
(7, 58, 0, '2020-06-03 03:35:38', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tiene`
--

CREATE TABLE `tiene` (
  `id_publicidad_fk` int(10) NOT NULL,
  `id_publicacion_fk` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `userbe`
--

CREATE TABLE `userbe` (
  `id_admin` int(10) NOT NULL,
  `rol` varchar(100) NOT NULL,
  `username` varchar(45) NOT NULL,
  `apellido` varchar(45) DEFAULT NULL,
  `correo` varchar(45) DEFAULT NULL,
  `pass` varchar(45) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `estado` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `userbe`
--

INSERT INTO `userbe` (`id_admin`, `rol`, `username`, `apellido`, `correo`, `pass`, `nombre`, `estado`) VALUES
(1, 'general_admin', 'robertito', 'sandia', 'robertito@sandia.com', 'watermelon', 'roberto', 0),
(3, 'Suscription Admin', 'CapitanAmerica', 'Rogers', 'Steve.Roger@avengers.com', '63982e54a7aeb0d89910475ba6dbd3ca6dd4e5a1', 'Steve', 0),
(4, 'Publicity Admin', 'Spiderman', 'Parker', 'peter@parker.com', 'adcd7048512e64b48da55b027577886ee5a36350', 'Peter', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `id_cuenta_fk` int(10) NOT NULL,
  `nom_user` varchar(100) NOT NULL,
  `ape_user` varchar(100) NOT NULL,
  `fec_nac` date NOT NULL,
  `sex_user` tinyint(4) NOT NULL DEFAULT 0 COMMENT '0 indefinido, 1 Mujer, 2 Hombre, 3 Otro'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`id_cuenta_fk`, `nom_user`, `ape_user`, `fec_nac`, `sex_user`) VALUES
(6, 'Ganzo', 'Pelufo', '2000-02-04', 1),
(7, 'Eugenio', 'Garzon', '2018-02-06', 1),
(9, 'Burro', 'Shreck', '1980-11-22', 1),
(11, 'Camilo', 'Cosentino', '1971-08-13', 1),
(12, 'elver', 'gudogudo', '1977-07-15', 1),
(13, 'Jose', 'Perez', '1980-01-01', 1),
(14, 'Roberto', 'Piedra', '1990-11-11', 1),
(15, 'pega', 'joso', '1990-02-22', 1),
(16, 'pantera', 'rosa', '1111-11-11', 1),
(17, 'eloisa', 'didid', '0111-11-11', 1),
(18, 'Pancho', 'Pito', '2018-07-10', 1),
(19, 'Paola', 'Bianco', '3333-02-22', 1),
(20, 'peep', 'guerra', '1990-03-12', 2),
(21, 'caqhito', 'ca', '2004-07-04', 0),
(22, 'fanatico', 'defobal', '2015-07-10', 2),
(23, 'Pedro', 'Borges', '1990-03-03', 1),
(24, 'Fabián', 'Pérez', '2000-04-04', 2),
(25, 'Juancho', 'Talarga', '1990-11-11', 2),
(26, 'Caramelos', 'Chupetines', '1234-02-12', 2),
(27, 'Capitan', 'America', '1993-04-14', 2),
(28, 'Make', 'Lele', '1984-05-16', 2),
(31, 'wwwww', 'www', '1111-11-11', 1),
(39, 'Roberto', 'Muso', '1991-11-11', 2),
(43, 'Anónimo', 'perez', '2000-08-09', 2),
(44, 'Pato', 'Aparato', '1976-07-11', 1),
(46, 'ricardo', 'galan', '2000-02-22', 3),
(48, 'ricardo', 'galan', '1990-08-01', 3),
(49, 'ricardo', 'galan', '1990-08-01', 3),
(50, 'Pato', 'Aparato', '1990-06-12', 2),
(51, 'Pato', 'fdasfa', '1987-07-22', 1),
(52, 'cacho', 'pelado', '1980-08-08', 1),
(53, 'fabian', 'peror', '1996-09-01', 2),
(57, 'cacho', 'peralta', '1998-11-11', 0),
(59, 'cholo', 'chico', '1900-12-22', 2),
(60, 'pireta', 'alex', '1999-02-02', 1),
(61, 'perro', 'Verde', '1990-11-10', 2);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `administra`
--
ALTER TABLE `administra`
  ADD PRIMARY KEY (`id_admin_fk`,`id_cuenta_fk`),
  ADD KEY `administra_ibfk_2` (`id_cuenta_fk`);

--
-- Indices de la tabla `autoriza`
--
ALTER TABLE `autoriza`
  ADD PRIMARY KEY (`id_admin_fk`,`id_suscrip_fk`),
  ADD KEY `autoriza_ibfk_2` (`id_suscrip_fk`);

--
-- Indices de la tabla `comenta`
--
ALTER TABLE `comenta`
  ADD PRIMARY KEY (`id_comenta`),
  ADD KEY `comenta_ibfk_1` (`id_cuenta_fk`),
  ADD KEY `comenta_ibfk_2` (`id_publicacion_fk`);

--
-- Indices de la tabla `comparte`
--
ALTER TABLE `comparte`
  ADD PRIMARY KEY (`id_cuenta_fk`,`id_publicacion_fk`),
  ADD KEY `comparte_ibfk_2` (`id_publicacion_fk`);

--
-- Indices de la tabla `controla`
--
ALTER TABLE `controla`
  ADD PRIMARY KEY (`id_admin_fk`,`id_publicacion_fk`),
  ADD KEY `id_publicacion_fk` (`id_publicacion_fk`);

--
-- Indices de la tabla `cuenta`
--
ALTER TABLE `cuenta`
  ADD PRIMARY KEY (`id_cuenta`),
  ADD UNIQUE KEY `mailUser` (`mail_cuenta`);

--
-- Indices de la tabla `es_amigo`
--
ALTER TABLE `es_amigo`
  ADD PRIMARY KEY (`id_cuenta_fk_u1`,`id_cuenta_fk_u2`),
  ADD KEY `id_cuenta_fk_u2` (`id_cuenta_fk_u2`);

--
-- Indices de la tabla `es_miembro`
--
ALTER TABLE `es_miembro`
  ADD PRIMARY KEY (`id_grupo_fk`,`id_cuenta_fk`),
  ADD KEY `id_cuenta_fk` (`id_cuenta_fk`);

--
-- Indices de la tabla `gestiona`
--
ALTER TABLE `gestiona`
  ADD PRIMARY KEY (`id_admin_fk`,`id_publicidad_fk`),
  ADD KEY `id_publicidad_fk` (`id_publicidad_fk`);

--
-- Indices de la tabla `grupo`
--
ALTER TABLE `grupo`
  ADD PRIMARY KEY (`id_grupo`);

--
-- Indices de la tabla `grupo_comparte`
--
ALTER TABLE `grupo_comparte`
  ADD PRIMARY KEY (`id_grupo_fk`,`id_publicacion_fk`,`id_cuenta_fk`),
  ADD KEY `grupo_comparte_ibfk_2` (`id_publicacion_fk`),
  ADD KEY `grupo_comparte_ibfk_3` (`id_cuenta_fk`);

--
-- Indices de la tabla `gusta`
--
ALTER TABLE `gusta`
  ADD PRIMARY KEY (`id_cuenta_fk`,`id_publicacion_fk`),
  ADD KEY `gusta_ibfk_1` (`id_cuenta_fk`),
  ADD KEY `id_publicacion_fk` (`id_publicacion_fk`);

--
-- Indices de la tabla `notifica`
--
ALTER TABLE `notifica`
  ADD PRIMARY KEY (`id_notifica`),
  ADD KEY `notificacion_ibfk_1` (`id_de_fk`),
  ADD KEY `notificacion_ibfk_2` (`id_para_fk`),
  ADD KEY `notificacion_ibfk_3` (`id_publicacion_fk`);

--
-- Indices de la tabla `pagina`
--
ALTER TABLE `pagina`
  ADD PRIMARY KEY (`id_cuenta_fk`);

--
-- Indices de la tabla `publicacion`
--
ALTER TABLE `publicacion`
  ADD PRIMARY KEY (`id_publicacion`);

--
-- Indices de la tabla `publicidad`
--
ALTER TABLE `publicidad`
  ADD PRIMARY KEY (`id_publicidad`);

--
-- Indices de la tabla `sigue`
--
ALTER TABLE `sigue`
  ADD PRIMARY KEY (`id_cuenta_p`,`id_cuenta_u`),
  ADD KEY `sigue_ibfk_2` (`id_cuenta_u`);

--
-- Indices de la tabla `suscripcion`
--
ALTER TABLE `suscripcion`
  ADD PRIMARY KEY (`id_suscrip`,`id_cuenta_fk`),
  ADD KEY `suscripcion_ibfk_1` (`id_cuenta_fk`);

--
-- Indices de la tabla `tiene`
--
ALTER TABLE `tiene`
  ADD PRIMARY KEY (`id_publicidad_fk`,`id_publicacion_fk`),
  ADD KEY `id_publicacion_fk` (`id_publicacion_fk`);

--
-- Indices de la tabla `userbe`
--
ALTER TABLE `userbe`
  ADD PRIMARY KEY (`id_admin`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id_cuenta_fk`),
  ADD KEY `id_cuenta_fk` (`id_cuenta_fk`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `comenta`
--
ALTER TABLE `comenta`
  MODIFY `id_comenta` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT de la tabla `cuenta`
--
ALTER TABLE `cuenta`
  MODIFY `id_cuenta` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;

--
-- AUTO_INCREMENT de la tabla `grupo`
--
ALTER TABLE `grupo`
  MODIFY `id_grupo` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT de la tabla `notifica`
--
ALTER TABLE `notifica`
  MODIFY `id_notifica` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=548;

--
-- AUTO_INCREMENT de la tabla `publicacion`
--
ALTER TABLE `publicacion`
  MODIFY `id_publicacion` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=305;

--
-- AUTO_INCREMENT de la tabla `publicidad`
--
ALTER TABLE `publicidad`
  MODIFY `id_publicidad` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `suscripcion`
--
ALTER TABLE `suscripcion`
  MODIFY `id_suscrip` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `userbe`
--
ALTER TABLE `userbe`
  MODIFY `id_admin` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `administra`
--
ALTER TABLE `administra`
  ADD CONSTRAINT `administra_ibfk_1` FOREIGN KEY (`id_admin_fk`) REFERENCES `userbe` (`id_admin`) ON UPDATE CASCADE,
  ADD CONSTRAINT `administra_ibfk_2` FOREIGN KEY (`id_cuenta_fk`) REFERENCES `cuenta` (`id_cuenta`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `autoriza`
--
ALTER TABLE `autoriza`
  ADD CONSTRAINT `autoriza_ibfk_1` FOREIGN KEY (`id_admin_fk`) REFERENCES `userbe` (`id_admin`) ON UPDATE CASCADE,
  ADD CONSTRAINT `autoriza_ibfk_2` FOREIGN KEY (`id_suscrip_fk`) REFERENCES `suscripcion` (`id_suscrip`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `comenta`
--
ALTER TABLE `comenta`
  ADD CONSTRAINT `comenta_ibfk_1` FOREIGN KEY (`id_cuenta_fk`) REFERENCES `cuenta` (`id_cuenta`) ON UPDATE CASCADE,
  ADD CONSTRAINT `comenta_ibfk_2` FOREIGN KEY (`id_publicacion_fk`) REFERENCES `publicacion` (`id_publicacion`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `comparte`
--
ALTER TABLE `comparte`
  ADD CONSTRAINT `comparte_ibfk_1` FOREIGN KEY (`id_cuenta_fk`) REFERENCES `cuenta` (`id_cuenta`) ON UPDATE CASCADE,
  ADD CONSTRAINT `comparte_ibfk_2` FOREIGN KEY (`id_publicacion_fk`) REFERENCES `publicacion` (`id_publicacion`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `controla`
--
ALTER TABLE `controla`
  ADD CONSTRAINT `controla_ibfk_1` FOREIGN KEY (`id_admin_fk`) REFERENCES `userbe` (`id_admin`) ON UPDATE CASCADE,
  ADD CONSTRAINT `controla_ibfk_2` FOREIGN KEY (`id_publicacion_fk`) REFERENCES `publicacion` (`id_publicacion`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `es_amigo`
--
ALTER TABLE `es_amigo`
  ADD CONSTRAINT `es_amigo_ibfk_1` FOREIGN KEY (`id_cuenta_fk_u1`) REFERENCES `cuenta` (`id_cuenta`),
  ADD CONSTRAINT `es_amigo_ibfk_2` FOREIGN KEY (`id_cuenta_fk_u2`) REFERENCES `cuenta` (`id_cuenta`);

--
-- Filtros para la tabla `es_miembro`
--
ALTER TABLE `es_miembro`
  ADD CONSTRAINT `es_miembro_ibfk_1` FOREIGN KEY (`id_cuenta_fk`) REFERENCES `usuario` (`id_cuenta_fk`),
  ADD CONSTRAINT `es_miembro_ibfk_2` FOREIGN KEY (`id_grupo_fk`) REFERENCES `grupo` (`id_grupo`);

--
-- Filtros para la tabla `gestiona`
--
ALTER TABLE `gestiona`
  ADD CONSTRAINT `gestiona_ibfk_1` FOREIGN KEY (`id_admin_fk`) REFERENCES `userbe` (`id_admin`),
  ADD CONSTRAINT `gestiona_ibfk_2` FOREIGN KEY (`id_publicidad_fk`) REFERENCES `publicidad` (`id_publicidad`);

--
-- Filtros para la tabla `grupo_comparte`
--
ALTER TABLE `grupo_comparte`
  ADD CONSTRAINT `grupo_comparte_ibfk_1` FOREIGN KEY (`id_grupo_fk`) REFERENCES `es_miembro` (`id_grupo_fk`) ON UPDATE CASCADE,
  ADD CONSTRAINT `grupo_comparte_ibfk_2` FOREIGN KEY (`id_publicacion_fk`) REFERENCES `publicacion` (`id_publicacion`) ON UPDATE CASCADE,
  ADD CONSTRAINT `grupo_comparte_ibfk_3` FOREIGN KEY (`id_cuenta_fk`) REFERENCES `es_miembro` (`id_cuenta_fk`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `gusta`
--
ALTER TABLE `gusta`
  ADD CONSTRAINT `gusta_ibfk_1` FOREIGN KEY (`id_cuenta_fk`) REFERENCES `cuenta` (`id_cuenta`) ON UPDATE CASCADE,
  ADD CONSTRAINT `gusta_ibfk_2` FOREIGN KEY (`id_publicacion_fk`) REFERENCES `publicacion` (`id_publicacion`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `notifica`
--
ALTER TABLE `notifica`
  ADD CONSTRAINT `notificacion_ibfk_1` FOREIGN KEY (`id_de_fk`) REFERENCES `cuenta` (`id_cuenta`),
  ADD CONSTRAINT `notificacion_ibfk_2` FOREIGN KEY (`id_para_fk`) REFERENCES `cuenta` (`id_cuenta`),
  ADD CONSTRAINT `notificacion_ibfk_3` FOREIGN KEY (`id_publicacion_fk`) REFERENCES `publicacion` (`id_publicacion`);

--
-- Filtros para la tabla `pagina`
--
ALTER TABLE `pagina`
  ADD CONSTRAINT `pagina_ibfk_1` FOREIGN KEY (`id_cuenta_fk`) REFERENCES `cuenta` (`id_cuenta`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `sigue`
--
ALTER TABLE `sigue`
  ADD CONSTRAINT `sigue_ibfk_1` FOREIGN KEY (`id_cuenta_p`) REFERENCES `pagina` (`id_cuenta_fk`) ON UPDATE CASCADE,
  ADD CONSTRAINT `sigue_ibfk_2` FOREIGN KEY (`id_cuenta_u`) REFERENCES `usuario` (`id_cuenta_fk`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `suscripcion`
--
ALTER TABLE `suscripcion`
  ADD CONSTRAINT `suscripcion_ibfk_1` FOREIGN KEY (`id_cuenta_fk`) REFERENCES `pagina` (`id_cuenta_fk`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `tiene`
--
ALTER TABLE `tiene`
  ADD CONSTRAINT `tiene_ibfk_1` FOREIGN KEY (`id_publicidad_fk`) REFERENCES `publicidad` (`id_publicidad`) ON UPDATE CASCADE,
  ADD CONSTRAINT `tiene_ibfk_2` FOREIGN KEY (`id_publicacion_fk`) REFERENCES `publicacion` (`id_publicacion`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD CONSTRAINT `usuario_ibfk_1` FOREIGN KEY (`id_cuenta_fk`) REFERENCES `cuenta` (`id_cuenta`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
